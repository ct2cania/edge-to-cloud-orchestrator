//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Updated on 04 Jun 2021
// Updated on 04 Jun 2021
//
// @author: ATOS
//
package controllers

import (
	"atos.pledger/e2c-orchestrator/rest-api/sec/models"
	"atos.pledger/e2c-orchestrator/rest-api/sec/auth"
	log "atos.pledger/e2c-orchestrator/common/logs"
	db "atos.pledger/e2c-orchestrator/data/db"
	
	"github.com/gin-gonic/gin"
)

// path used in logs
const pathLOG string = "E2CO > Rest-API > SEC > Controllers "

/* 
LoginResponse token response
*/
type LoginResponse struct {
	Token string `json:"token"`
}

/* 
Signup creates a user in db
*/
func Signup(c *gin.Context) {
	log.Info(pathLOG+"[Signup] Creating new user ...")

	var user models.User

	err := c.ShouldBindJSON(&user)
	if err != nil {
		log.Error(pathLOG+"[Signup] ", err)

		c.JSON(400, gin.H{
			"msg": "invalid json",
		})
		c.Abort()

		return
	}

	err = user.HashPassword(user.Password)
	if err != nil {
		log.Error(pathLOG+"[Signup] "+err.Error())

		c.JSON(500, gin.H{
			"msg": "error hashing password",
		})
		c.Abort()

		return
	}

	err = db.Signup(user)
	if err != nil {
		log.Error(pathLOG+"[Signup] ", err)

		c.JSON(500, gin.H{
			"msg": "error creating user: " + err.Error(),
		})
		c.Abort()

		return
	}

	c.JSON(200, user)
}

/* 
Login logs users in
*/
func Login(c *gin.Context) {
	log.Info(pathLOG+"[Login] Login ...")

	var payload models.LoginPayload
	var user models.User

	err := c.ShouldBindJSON(&payload)
	if err != nil {
		log.Error(pathLOG+"[Login] ", err)
		
		c.JSON(400, gin.H{
			"msg": "invalid json",
		})
		c.Abort()
		return
	}

	user, err = db.Login(payload) // Login(u models.LoginPayload) (models.User, error)
	if err != nil {
		c.JSON(401, gin.H{
			"msg": "user not found / invalid user credentials",
		})
		c.Abort()
		return
	}

	err = user.CheckPassword(payload.Password)
	if err != nil {
		log.Error(pathLOG+"[Login] ", err)
		c.JSON(401, gin.H{
			"msg": "invalid user credentials",
		})
		c.Abort()
		return
	}

	jwtWrapper := auth.JwtWrapper{
		SecretKey:       "verysecretkey",
		Issuer:          "AuthService",
		ExpirationHours: 8760,
	}

	signedToken, err := jwtWrapper.GenerateToken(user.Email)
	if err != nil {
		log.Error(pathLOG+"[Login] ", err)
		c.JSON(500, gin.H{
			"msg": "error signing token",
		})
		c.Abort()
		return
	}

	tokenResponse := LoginResponse{
		Token: signedToken,
	}

	c.JSON(200, tokenResponse)

	return
}