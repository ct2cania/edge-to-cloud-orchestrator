//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 05 Aug 2021
//
// @author: ATOS
//
package eventhandler

import (
	"strconv"

	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/db"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/http"
	"atos.pledger/e2c-orchestrator/common/cfg"
)

/*
	1. From Kafka:

		```json
		{
			"name_slice": "my_namespace",
			"requests_cpu":"2",
			"user_id":"620bd30a4dfac900093a8bac",
			"compute_id":"620bd42a4dfac90008904f37",
			"limits_memory":"2",
			"placeholders":[],
			"id":8,
			"requests_memory":"2",
			"operation":"provision",
			"entity":"infrastructure",
			"limits_cpu":"2"
		}
		```

	2. Call to SOE: **POST /compute_chunk** 
						
		Request body (application/json):
		
		```json
		{   
			"user_id": "5b63089158f568073093f70d",   
			"compute_id": "5b63089158f568073093f70d",   
			"name": "pledger-compute-chunk",   
			"requirements": {     
				"ram": {       
					"required": 1000,       
					"limits": 2000,       
					"units": "Mi"     
				},     
				"cpus": {       
					"required": 100       
					"limits": 200,       
					"units": "m"     
				},     
				"storage": {       
					"required": 100,       
					"limits": 200,      
					"units": "GB"    
				}   
			} 
		} 
		```
		
		=> return *compute_chunk_id*
	
	3. Call to SOE: **POST /slic3**
		
		Request body (application/json):
		
		```json
		{   
			"user_id": "5b63089158f568073093f70d",   
			"name": "nova",   
			"chunk_ids": [     
				"6b63089158f568073093f70a", "6b63089158f568073093f70b"   
			] 
		}  
		```
	
		=> return *slic3_id*
*/

// soeConfigEventsLoop Kafka events handler for SOE changes
func soeConfigEventsLoop() {
	
} // soeConfigEventsLoop()

// processCreateComputeChunkAndSlice Create Compute Chunk
/*
	eventInfoMap:

	```json
	{
		"name_slice": "my_namespace",
		"requests_cpu":"2",
		"user_id":"620bd30a4dfac900093a8bac",
		"compute_id":"620bd42a4dfac90008904f37",
		"limits_memory":"2",
		"placeholders":[],
		"id":8,
		"requests_memory":"2",
		"operation":"provision",
		"entity":"infrastructure",
		"limits_cpu":"2"
	}
	```
*/
func processCreateComputeChunkAndSlice(id string, eventInfoMap map[string]interface{}) error {
	log.Info(pathLOG + "[processCreateComputeChunkAndSlice] Creating Compute Chunk in SOE ...")

	orch, err := db.ReadOrchestrator(id) // get orchestrator: ([]structs.E2cOrchestrator, error)
	if err != nil {
		return err
	}

	sec := common.AuthTokenNeeded(orch)	// Auth Token needed?

	// 1. compute chunk
	soeObj, err := computeChunk(id, eventInfoMap, orch, sec)
	if err != nil {
		return err
	}

	// 2. call to slic3
	soeObj, err = slic3(id, eventInfoMap, soeObj, orch, sec)
	if err != nil {
		return err
	} else {
		// save network information in INFRASTRUCTURE !!!!
		_, err := db.ReadSOEObj(soeObj.IDE2cOrchestrator)
		if err != nil { // error or app not found
			log.Info(pathLOG + "[processCreateComputeChunkAndSlice] SOEObj not found. Creating new SOEObj ...")
			db.SetSOEObj(soeObj.IDE2cOrchestrator, soeObj) 
		} else { // found
			log.Info(pathLOG + "[processCreateComputeChunkAndSlice] SOEObj found. Updating existing SOEObj ...")
			db.UpdateSOEObj(soeObj.IDE2cOrchestrator, soeObj) 
		}
	}

	return err
}

// computeChunk
/*
	eventInfoMap:

	```json
	{
		"name_slice": "my_namespace",
		"requests_cpu":"2",
		"user_id":"620bd30a4dfac900093a8bac",
		"compute_id":"620bd42a4dfac90008904f37",
		"limits_memory":"2",
		"placeholders":[],
		"id":8,
		"requests_memory":"2",
		"operation":"provision",
		"entity":"infrastructure",
		"limits_cpu":"2"
	}
	```
*/
func computeChunk(id string, eventInfoMap map[string]interface{}, orch structs.E2cOrchestrator, sec bool) (structs.E2coSoeObject, error) {
	var soeObj structs.E2coSoeObject
	// UserID
	if eventInfoMap["user_id"] != nil {
		soeObj.UserID = eventInfoMap["user_id"].(string)
	}
	// ComputeID
	if eventInfoMap["compute_id"] != nil {
		soeObj.ComputeID = eventInfoMap["compute_id"].(string)
	}
	// Name ==> soeObj.Name = eventInfoMap["name_slice"].(string) // namespace!!!!
	if eventInfoMap["name_slice"] != nil {
		soeObj.Name = eventInfoMap["name_slice"].(string)
	}
	// IDE2cOrchestrator
	soeObj.IDE2cOrchestrator = id

	// RAM
	// requests_memory
	if eventInfoMap["requests_memory"] != nil {
		if i, err := strconv.ParseInt(eventInfoMap["requests_memory"].(string),10,64); err == nil {
			soeObj.Requirements.Ram.Required = int(i) // eventInfoMap["requests_memory"].(int)
		} else {
			log.Warn(pathLOG + "[computeChunk] 'requests_memory' value is not a number.")
		}
	} else {
		soeObj.Requirements.Ram.Required = 1
	}
	//  limits_memory
	if eventInfoMap["limits_memory"] != nil {
		if i, err := strconv.ParseInt(eventInfoMap["limits_memory"].(string),10,64); err == nil {
			soeObj.Requirements.Ram.Limits = int(i) //eventInfoMap["limits_memory"].(int)
		} else {
			log.Warn(pathLOG + "[computeChunk] 'limits_memory' value is not a number.")
		}
	} else {
		soeObj.Requirements.Ram.Limits = 1
	}
	//  units_memory
	if eventInfoMap["units_memory"] != nil {
		soeObj.Requirements.Ram.Units = eventInfoMap["units_memory"].(string)
	} else {
		soeObj.Requirements.Ram.Units = "Mi"
	}
	// Cpus
	// requests_cpu
	if eventInfoMap["requests_cpu"] != nil {
		if i, err := strconv.ParseInt(eventInfoMap["requests_cpu"].(string),10,64); err == nil {
			soeObj.Requirements.Cpus.Required = int(i) //eventInfoMap["requests_cpu"].(int)
		} else {
			log.Warn(pathLOG + "[computeChunk] 'requests_cpu' value is not a number.")
		}
	} else {
		soeObj.Requirements.Cpus.Required = 1
	}
	//  limits_cpu
	if eventInfoMap["limits_cpu"] != nil {
		if i, err := strconv.ParseInt(eventInfoMap["limits_cpu"].(string),10,64); err == nil {
			soeObj.Requirements.Cpus.Limits = int(i) //eventInfoMap["limits_cpu"].(int)
		} else {
			log.Warn(pathLOG + "[computeChunk] 'limits_cpu' value is not a number.")
		}
	} else {
		soeObj.Requirements.Cpus.Limits = 1
	}
	//  units_cpu
	if eventInfoMap["units_cpu"] != nil {
		soeObj.Requirements.Cpus.Units = eventInfoMap["units_cpu"].(string)
	} else {
		soeObj.Requirements.Cpus.Units = "m"
	}
	// STORAGE
	// requests_storage
	if eventInfoMap["requests_storage"] != nil {
		if i, err := strconv.ParseInt(eventInfoMap["requests_storage"].(string),10,64); err == nil {
			soeObj.Requirements.Storage.Required = int(i) //eventInfoMap["requests_storage"].(int)
		} else {
			log.Warn(pathLOG + "[computeChunk] 'requests_storage' value is not a number.")
		}
	} else {
		soeObj.Requirements.Storage.Required = 1
	}
	//  limits_storage
	if eventInfoMap["limits_storage"] != nil {
		if i, err := strconv.ParseInt(eventInfoMap["limits_storage"].(string),10,64); err == nil {
			soeObj.Requirements.Storage.Limits = int(i) //eventInfoMap["limits_storage"].(int)
		} else {
			log.Warn(pathLOG + "[computeChunk] 'limits_storage' value is not a number.")
		}
	} else {
		soeObj.Requirements.Storage.Limits = 1
	}
	//  units_storage
	if eventInfoMap["units_storage"] != nil {
		soeObj.Requirements.Storage.Units = eventInfoMap["units_storage"].(string)
	} else {
		soeObj.Requirements.Storage.Units = "GB"
	}

	// Call to SOE: **POST /compute_chunk** 
	log.Info(pathLOG + "[computeChunk] Creating a Compute Chunk in SOE ...")

	strSoeObj, err := common.StructToString(soeObj)
	if err == nil {
		log.Info(pathLOG + "[computeChunk] Body message: " + strSoeObj)
	}

	_, objRes, err := http.PostStruct(
		cfg.GetPathSOEComputeChunk(orch),
		sec,
		orch.ConnectionToken,
		soeObj)
	if err != nil {
		log.Error(pathLOG+"[computeChunk] ERROR", err)
		return soeObj, err
	}
	log.Info(pathLOG + "[computeChunk] RESPONSE: OK")

	// ==> objRes
	/*
		{
			"auth_url":null,
			"available_ext_net":false,
			"compute_id":"620bd42a4dfac90008904f37",
			"description":"620bd5754dfac900093a8bad",
			"id":"620f89cd4dfac9000bfb1580",
			"name":"620bd5754dfac900093a8bad",
			"os_project_id":null,
			"os_user_id":null,
			"osm_k8s_cluster_id":"1d72f15d-b44e-4a22-b383-481c3e75abfc",
			"osm_vim_id":"a3be741f-aa1b-4e06-b7d8-564b1063d78d",
			"password":null,
			"requirements": {
				"cpus": {"limits":2, "required":2," units":"m"},
				"ram": {"limits":2,"required":2, "units":"Mi"},
				"storage": {"limits":1,"required":1,"units":"GB"}
			},
			"user_id":"620bd30a4dfac900093a8bac",
			"username":null
		}

		=> return *compute_chunk_id* ==> "id" 
	*/

	str, err := common.StructToString(objRes)
	if err != nil {
		log.Error(pathLOG + "[computeChunk] Error: " + err.Error())
	} else {
		log.Info(pathLOG + "[computeChunk] RESPONSE: " + str)
	}

	// ChunkIDs
	if objRes["id"] != nil {
		soeObj.ChunkIDs = append(soeObj.ChunkIDs, objRes["id"].(string))
	}

	return soeObj, nil
}

/**
E2coSoeSliceObject used in call to SOE service: POST /slic3

	```json
	{   
		"user_id": "5b63089158f568073093f70d",   
		"name": "nova",   
		"chunk_ids": [     
			"6b63089158f568073093f70a", "6b63089158f568073093f70b"   
		] 
	}  
	```

	=> return *slic3_id*
*/
type E2coSoeSliceObject struct {
	UserID 				string 		`json:"user_id,omitempty"`
	Name 				string  	`json:"name,omitempty"`
	ChunkIDs  			[]string  	`json:"chunk_ids,omitempty"`
}

// slic3
func slic3(id string, eventInfoMap map[string]interface{}, soeObj structs.E2coSoeObject, orch structs.E2cOrchestrator, sec bool) (structs.E2coSoeObject, error) {
	var slic3Obj E2coSoeSliceObject

	slic3Obj.UserID = soeObj.UserID
	slic3Obj.Name = soeObj.Name
	slic3Obj.ChunkIDs = soeObj.ChunkIDs
	
	log.Info(pathLOG + "[slic3] Creating a new slice in SOE ...")
	_, objRes2, err := http.PostStruct(
		cfg.GetPathSOESlic3(orch),
		sec,
		orch.ConnectionToken,
		slic3Obj)
	if err != nil {
		log.Error(pathLOG+"[slic3] ERROR", err)
		return soeObj, err
	}
	
	str, err := common.StructToString(objRes2)
	if err != nil {
		log.Error(pathLOG + "[slic3] Error: " + err.Error())
	} else {
		log.Info(pathLOG + "[slic3] RESPONSE: " + str)
	}

	// TODO store slic3_id
	/*
		{
			"activation_status":"not-needed",
			"chunks":{
				"compute_chunk":[
					{
						"auth_url":null,
						"available_ext_net":false,
						"compute_id":"620bd42a4dfac90008904f37",
						"description":"namespace-a",
						"id":"621f47344dfac900093a8bb1",
						"name":"namespace-a",
						"os_project_id":null,
						"os_user_id":null,
						"osm_k8s_cluster_id":"13fb11da-f10e-4dfb-a97c-2071cacfebbf",
						"osm_vim_id":"3d7e68a4-0801-4c95-bed3-71914e5cd776",
						"password":null,
						"requirements":{
							"cpus":{"consumed":0,"required":2},
							"ram":{"consumed":0,"required":2,"units":"Mi"},
							"storage":{"consumed":0,"required":1,"units":"GB"}
						},
						"user_id":"620bd30a4dfac900093a8bac",
						"username":null
					}],
				"network_chunk":[],
				"radio_chunk":[]
			},
			"id":"621f47344dfac9000bfb1581",
			"name":"namespace-a",
			"radio_service":null,
			"user_id":"620bd30a4dfac900093a8bac"
		}
	*/
	if objRes2["id"] != nil {
		soeObj.Slic3ID = objRes2["id"].(string)
	}

	return soeObj, nil
}