//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 15 Mar 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package slaframework

import (
	cfg "atos.pledger/e2c-orchestrator/common/cfg"
	structs "atos.pledger/e2c-orchestrator/data/structs"
	log "atos.pledger/e2c-orchestrator/common/logs"
	http "atos.pledger/e2c-orchestrator/common/http"
	"strconv"
	"strings"
	"time"
)

///////////////////////////////////////////////////////////////////////////////

/*
createGuaranteesFromE2coQoS Creates Guarantees for COMPSs based tasks
*/
func createGuaranteesFromE2coQoS(task structs.E2coSubApp) []structs.SLADetailsGuarantee {
	log.Info(pathLOG + "[createGuaranteesFromE2coQoS] Generating guarantees from task qos ...")

	var guarantees []structs.SLADetailsGuarantee
	guarantees = make([]structs.SLADetailsGuarantee, 0) // guarantees list

	if len(task.Qos) > 0 {
		// []E2coQoS defined in task 
		for _, q := range task.Qos {
			// E2coQoS
			log.Info(pathLOG + "[createGuaranteesFromE2coQoS] Generating guarantees from E2coQoS defined in task [" + q.Name + "] ...")

			// guarantee:
			g := structs.SLADetailsGuarantee{}   
			/*
				Name       string       	   `json:"name"`
				Constraint string       	   `json:"constraint"`
				Penalties  []SLAPenaltyDef 	   `json:"penalties,omitempty"`
				Importance []SLAGuaranteeType  `json:"importance"`   		
				Actions    []SLAActionsDef 	   `json:"actions,omitempty"`	
			*/
			g.Name = q.Name
			g.Constraint = q.Constraint
			g.Penalties = q.Penalties
			g.Importance = q.Importance
			g.Actions = q.Actions

			guarantees = append(guarantees, g)
		}
	} 

	return guarantees
}

/*
createSLAgreementFromE2coQoS ...
*/
func createSLAgreementFromE2coQoS(task structs.E2coSubApp) (string, error) {
	log.Info(pathLOG + "[createSLAgreementFromE2coQoS] Generating SLA agrement for task [" + task.ID + "] ...")

	// Create uuid
	agreementID := strings.Replace(task.ID, "-", "_", -1)

	// Create SLA Agreement structure
	var jsonAgreement *structs.SLA
	jsonAgreement = new(structs.SLA)

	jsonAgreement.ID = agreementID
	jsonAgreement.Name = "agreement_" + agreementID
	jsonAgreement.State = "started"
	//jsonAgreement.Location = task.IDE2cOrchestrator
	jsonAgreement.Details.ID = agreementID
	jsonAgreement.Details.Name = "agreement_" + agreementID
	jsonAgreement.Details.Type = "agreement"
	jsonAgreement.Details.Provider.ID = "<Provider>"
	jsonAgreement.Details.Provider.Name = "<Provider>"
	jsonAgreement.Details.Client.ID = "<Client>"
	jsonAgreement.Details.Client.Name = "<Client>"
	jsonAgreement.Details.Creation = time.Now()
	jsonAgreement.Details.Expiration = time.Now().AddDate(1, 0, 0) // add 1 year

	// create guarantees
	resGuarantees := createGuaranteesFromE2coQoS(task)

	jsonAgreement.Details.Guarantees = resGuarantees

	// Call to SLALite API to create the agreement
	// ==> curl -k -X POST -d @agreement.json http://192.168.7.28.xip.io/agreements
	status, _, err := http.Post(
		cfg.Config.E2COSLALiteEndPoint+"/agreements",
		false,
		"",
		jsonAgreement)

	if err != nil {
		log.Error(pathLOG+"[createSLAgreementFromE2coQoS] ERROR", err)
		return "Error creating the SLA Agreement", err
	}

	log.Info(pathLOG + "[createSLAgreementFromE2coQoS] RESPONSE: OK (" + strconv.Itoa(status) + ")")

	return agreementID, nil
}
