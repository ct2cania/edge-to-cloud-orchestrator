//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 15 Mar 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package slaframework

import (
	log "atos.pledger/e2c-orchestrator/common/logs"
	cfg "atos.pledger/e2c-orchestrator/common/cfg"
	http "atos.pledger/e2c-orchestrator/common/http"
	structs "atos.pledger/e2c-orchestrator/data/structs"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// path used in logs
const pathLOG string = "E2CO > Connectors > SLA-Framework "

///////////////////////////////////////////////////////////////////////////////

/*
startAgreemnt Call to SLALite API to start the agreement
*/
func startAgreemnt(agreementID string) (string, error) {
	log.Info(pathLOG + "[startAgreemnt] Starting SLA agrement [" + agreementID + "] ...")
	// ==> curl -k -X PUT -d @agreement.json http://192.168.7.28.xip.io/agreements/a03/start
	data := url.Values{}
	status, _, err := http.Put(cfg.Config.E2COSLALiteEndPoint+"/agreements/"+agreementID+"/start", false, "", data)

	if err != nil {
		log.Error(pathLOG+"[startAgreemnt] ERROR", err)
		return "Error starting the SLA Agreement", err
	}
	log.Info(pathLOG + "[startAgreemnt] RESPONSE: OK")

	return strconv.Itoa(status), nil
}

// stopSLA
func stopSLA(agreementID string, tries int) (string, error) {
	status, _, err := http.Put(cfg.Config.E2COSLALiteEndPoint+"/agreements/"+agreementID+"/stop", false, "", url.Values{})
	if err != nil {
		log.Error(pathLOG+"[stopSLA] ERROR Trying to stop SLA ", err)

		if tries == 0 {
			return "Error stopping the SLA Agreement", err
		}

		log.Info(pathLOG + "[stopSLA] Trying to stop the SLA again in 15 seconds ...")

		time.Sleep(15 * time.Second)
		return stopSLA(agreementID, tries-1)
	}

	log.Info(pathLOG + "[stopSLA] RESPONSE: OK")
	return strconv.Itoa(status), nil
}

/*
stopAgreemnt Call to SLALite API to stop the agreement
*/
func stopAgreemnt(agreementID string) (string, error) {
	agreementID = strings.Replace(agreementID, "-", "_", -1)
	log.Info(pathLOG + "[stopAgreemnt] Stopping SLA agrement [" + agreementID + "] ...")
	return stopSLA(agreementID, 3)
}

/*
terminateAgreemnt Call to SLALite API to terminate the agreement
*/
func terminateAgreemnt(agreementID string) (string, error) {
	agreementID = strings.Replace(agreementID, "-", "_", -1)
	log.Info(pathLOG + "[terminateAgreemnt] Terminating SLA agrement [" + agreementID + "] ...")

	// ==> curl -k -X PUT -d @agreement.json http://192.168.7.28.xip.io/agreements/a03/terminate
	data := url.Values{}
	status, _, err := http.Put(cfg.Config.E2COSLALiteEndPoint+"/agreements/"+agreementID+"/terminate", false, "", data)

	if err != nil {
		log.Error(pathLOG+"[terminateAgreemnt] ERROR", err)
		return "Error terminating the SLA Agreement", err
	}
	log.Info(pathLOG + "[terminateAgreemnt] RESPONSE: OK")

	return strconv.Itoa(status), nil
}

///////////////////////////////////////////////////////////////////////////////

/*
CreateStartSLAFromE2coQoS creates and starts an SLA
*/
func CreateStartSLAFromE2coQoS(app structs.E2coSubApp) error {
	log.Info(pathLOG + "[CreateStartSLAFromE2coQoS] Creating SLA agreement ...")
	agreementID, err := createSLAgreementFromE2coQoS(app)
	if err != nil {
		log.Error(pathLOG + "[CreateStartSLAFromE2coQoS] ERROR creating SLA Agreement")
	} else {
		log.Info(pathLOG + "[CreateStartSLAFromE2coQoS] Starting SLA agreement " + agreementID + " ...")
		_, err = startAgreemnt(agreementID)
		if err != nil {
			log.Error(pathLOG + "[CreateStartSLAFromE2coQoS] ERROR starting SLA Agreement")
		} else {
			log.Info(pathLOG + "[CreateStartSLAFromE2coQoS] SLA agreement " + agreementID + " started")
		}
	}

	return err
}


/*
StopTerminateSLA stops and terminates an SLA agreement after TASK is successfully deployed
*/
func StopTerminateSLA(taskID string) {
	log.Info(pathLOG + "[StopTerminateSLA] Stopping SLA agreement from task [" + taskID + "] ...")

	agreementID := taskID

	// TODO agreement id to DB
	_, err := stopAgreemnt(agreementID)

	if err != nil {
		log.Error(pathLOG + "[StopTerminateSLA] ERROR Stopping SLA with id = " + agreementID)
	} else {
		log.Info(pathLOG + "[StopTerminateSLA] Terminating SLA agreement " + agreementID + " ...")
		_, err := terminateAgreemnt(agreementID)
		if err != nil {
			log.Info(pathLOG + "[StopTerminateSLA] ERROR Terminating SLA Agreement")
		} else {
			log.Info(pathLOG + "[StopTerminateSLA] SLA agreement " + agreementID + " terminated")
		}
	}
}

