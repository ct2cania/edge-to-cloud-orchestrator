//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 15 Mar 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package connectors

import (
	structs "atos.pledger/e2c-orchestrator/data/structs"
	slaframework "atos.pledger/e2c-orchestrator/connectors/sla-framework"
	log "atos.pledger/e2c-orchestrator/common/logs"
)

// path used in logs
const pathLOG string = "E2CO > Connectors "

/*
CreateSLA creates and starts the SLA
*/
func CreateSLA(task structs.E2coSubApp) error {
	if task.Qos != nil && len(task.Qos) > 0 {
		log.Info(pathLOG + "[CreateSLA] Creating SLA from QoS parameters ...")
		return slaframework.CreateStartSLAFromE2coQoS(task)
	}

	log.Warn(pathLOG + "[CreateSLA] No QoS defined")
	return nil
}

/*
TerminateSLA stops and terminates the SLA
*/
func TerminateSLA(taskID string) {
	slaframework.StopTerminateSLA(taskID)
}