//
// Copyright 2017 Atos
//
// SLA-FRAMEWORK application
// PLEDGER Project: http://www.pledger-project.eu/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 10 Sep 2021
// Updated on 10 Sep 2021
//
// @author: ATOS
//
package kafka


// path used in logs
const pathLOG string = "E2CO > Connectors > Kafka "

/*
Adapter generic adapter interface
*/
type Adapter interface {
	NewClient()
	CheckForMessages(topic string) (string, error)
	WriteMessage(topic string, message string) (string, error)
}