//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 19 Mar 2021
//
// @author: ATOS
//
import React, { Component } from "react";
import request from "request";
import { Alert, Spinner, Button, Tabs, TabPane, OverlayTrigger, Tooltip, Badge, Form, Row, Col, Dropdown } from 'react-bootstrap';
import vis from "vis-network";


/**
 * AppDeployment
 */
class Apps extends Component {


  /**
   * CONSTRUCTOR
   */
  constructor(props, context) {
    super(props, context);

    this.state = {
      isLoading: false,
      serv_def: "{}",
      total_apps_1: 0,
      sel_service_instance_id_1: "",
      job_def: "",
      start_si_button: false,
      report_si_button: false,
      cancel_si_button: false,
      msg: "",
      msg_content: "",
      show_alert: false,
      show_info: false,
      msg2: "",
      msg_content2: "",
      show_alert2: false,
      show_info2: false,
      job_report: "",

      // app
      app_id: "",
      app_type: "",
      app_description: "",
      app_name: "",
      app_replicas: "",
      app_cluster: "",
      app_created: "",
      app_status: "",

      notCreatingNew: true,

      f: {
        replicas: 1
      },
      apps: []
    };

    this.newDeployment = this.newDeployment.bind(this);
    this.cancel = this.cancel.bind(this);
    this.clear = this.clear.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleView2 = this.handleView2.bind(this);
    this.handleSelectApp = this.handleSelectApp.bind(this);
    this.loadAppsList = this.loadAppsList.bind(this);
  }

  /*
  componentDidMount
  */
  componentDidMount() {
    this.handleView2(null);
    this.loadAppsList(null);
  }

  // 
  newDeployment() {
    this.setState({ notCreatingNew: false});
  }

  // 
  cancel() {
    this.setState({ notCreatingNew: true});
  }

  // 
  clear() {
    this.setState({app_id: "",
                  app_type: "",
                  app_description: "",
                  app_name: "",
                  app_replicas: "",
                  app_cluster: "",
                  app_created: "",
                  app_status: ""});
  }

  // Remove application
  handleDelete(event) {
    this.setState({isLoading: true});
    console.log("Launching a new application in Pledger platform (form tab) ...");

    // call to api
    try {
      var that = this;
      var formData = JSON.parse(this.state.serv_def);
      console.log(formData);
      console.log(global.rest_api_lm + "apps");

      request.post({url: global.rest_api_lm + "apps", json: formData}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert2: true, msg2: "POST /api/v1/apps", msg_content2: err.toString() });
        }
        else {
          console.log("Launching a new application in Pledger platform ... ok");
          if (global.debug) {
            body = JSON.stringify(body);
            that.setState({ show_info2: true, msg2: "POST /api/v1/apps => " + resp.statusCode, msg_content2: "Application deployed: response: " + body });
          }
        }

        that.setState({isLoading: false});
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert2: true, msg: "POST /api/v1/apps", msg_content2: err.toString(), isLoading: false });
    }
  }

  // 
  onDismiss() {
    this.setState({ show_alert: false });
    this.setState({ show_info: false });
    this.setState({ msg: "" });
    this.setState({ msg_content: "" });
  }

  // 
  onDismiss2() {
    this.setState({ show_aler2t: false });
    this.setState({ show_info2: false });
    this.setState({ msg: "" });
    this.setState({ msg_content2: "" });
  }



  // 
  handleSelectApp(event) {
    console.log(event);
  }

  /**
   *
   */
  loadAppsList(event) {
    console.log("Getting applications report ...");
    try {
      var that = this;
      request.get({url: global.rest_api_lm + "apps"}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "GET /api/v1/apps", msg_content: err.toString() });
          that.setState({ apps: []});
        }
        else {
          console.log('Getting report ... ok');
          try {
            console.log(body);
            if (JSON.parse(body).list == null) {
              that.setState({ apps: []});
            } else {
              that.setState({ apps: JSON.parse(body).list});
            }
          }
          catch(err) {
            console.error(err);
            that.setState({ apps: []});
          }
        }
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "GET /api/v1/apps", msg_content: err.toString() });
      that.setState({ apps: []});
    }
  }


  /**
   * Get app info (call to api)
   */
  getApp() {
    try {
      console.log('Getting app ...');
      var that = this;
      request.get({url: global.rest_api_lm + '/apps/' + that.state.sel_service_instance_1}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "GET /apps/" + that.state.sel_service_instance_1, msg_content: err.toString() });
        }
        else {
          if (resp.statusCode == 200) {
            console.log('Getting app ... ok');
            body = JSON.parse(body);
            console.log(body);
            that.setState({app_id: body['object']['id'],
                          app_type: body['object']['type'],
                          app_description: body['object']['description'],
                          app_name: body['object']['name'],
                          app_replicas: body['object']['replicas'],
                          app_cluster: body['object']['idE2cOrchestrator'],
                          app_created: body['object']['created'],
                          app_status: body['object']['status']});

            
            if (body['object'] != null) {
              console.log(body['object']);
              if (body['object']['service_type'] == "compss") {
                that.setState({ start_si_button: true, report_si_button: true, cancel_si_button: true });
              } else {
                that.setState({ start_si_button: false, report_si_button: false, cancel_si_button: false });
              }
            }
          }
          else {
            that.setState({ show_alert: true, msg: "GET /api/v1/apps/{id}", msg_content: JSON.stringify(body) + " => " + resp.statusCode });
          }
        }
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "GET /api/v1/apps/{id}", msg_content: err.toString() });
    }
  }


  /**
   * Load initial graph with all applications managed by E2CO
   */
  handleView2(event) {
    this.setState({isLoading: true});
    console.log('Getting apps ...');
    // call to api
    try {
      var that = this;
      request.get({url: global.rest_api_lm + "ime/e2co-apps"}, function(err, resp, body) {
        if (err) {
          console.error(err);
          that.setState({ show_alert: true, msg: "GET /api/v1/ime/e2co-apps", msg_content: err.toString() });
          that.setState({ apps: []});
        }
        else {
          if (resp.statusCode == 200) {
            console.log('Getting apps ... ok');

            body = JSON.parse(body);
            console.log(body);

            ////////////////////////////////////////////////////////////////////////////
            if (body['list'] != null && body['list'].length > 0) {
              // create an array with nodes
              var nodes2 = new vis.DataSet([
                {id: 'ag_1', label: "<b>E2C-Orchestrator</b>", image: './vendor/img/logo2.png', shape: 'image', title: "E2CO application",
                 font: {size:13, multi: true, color: "black", strokeWidth:2, strokeColor: "#dddddd"}, level: 0} 
              ]);

              var edges2 = new vis.DataSet([]);
              var total_apps = 0;

              // ORCHESTRATORS / CLUSTERS
              body['list'].forEach(function(element) {
                var ncolor = "white";
                if (element['status'] == "started") {
                  ncolor = "lightgreen";
                } else if (element['status'] == "error") {
                  ncolor = "lightred";
                }

                if (element['type'].toLowerCase() == "k8s") {
                  nodes2.add({id: element['id'], label: element['id'] + "    (<i>" + element['ip'] + "</i>)", 
                              image: './vendor/img/K8s.png', shape: 'image',
                              font: {size:11, multi: true, color: ncolor}, level: 1});
                } else if (element['type'].toLowerCase() == "microk8s") {
                  nodes2.add({id: element['id'], label: element['id'] + "    (<i>" + element['ip'] + "</i>)", 
                              image: './vendor/img/microk8s_cluster.png', shape: 'image',
                              font: {size:11, multi: true, color: ncolor}, level: 1});
                } else if (element['type'].toLowerCase() == "docker") {
                  nodes2.add({id: element['id'], label: element['id'] + "    (<i>" + element['ip'] + "</i>)", 
                              image: './vendor/img/docker-logo.png', shape: 'image',
                              font: {size:11, multi: true, color: ncolor}, level: 1});
                } else {
                  nodes2.add({id: element['id'], label: element['id'], image: './vendor/img/apps_started_mini.png', shape: 'image',
                              font: {size:11, multi: true, color: ncolor}, level: 1});
                }

                edges2.add({from: 'ag_1', to: element['id'], color:{color:"white"}, dashes: [2,2,10,10]});

                // APPLICATIONS
                if (element['apps'] != null) {
                  element['apps'].forEach(function(appelement) {
                    nodes2.add({id: 'app'+appelement['id'], label: appelement['name'], image: 'vendor/img/apps_mini.png', shape: 'circularImage',
                                font: {size:11, multi: true, color: "white"}, level: 1, opacity: 0.7});
  
                    if (element['status'] == "deployed") {
                      edges2.add({from: element['id'], to: 'app'+appelement['id'], color:{color:"lightgreen"}, dashes: true});
                    } else if (element['status'] == "error") {
                      edges2.add({from: element['id'], to: 'app'+appelement['id'], color:{color:"lightred"}, dashes: true});
                    } else {
                      edges2.add({from: element['id'], to: 'app'+appelement['id'], color:{color:"white"}, dashes: [2,2,10,10]});
                    }
  
                    total_apps++;
                  });
                }
                
              });

              // create a network2
              var container2 = document.getElementById('mynetwork3');
              var data2 = {
                nodes: nodes2,
                edges: edges2
              };
              var options2 = {
                nodes: {
                  size:15
                }
              };
              var network2 = new vis.Network(container2, data2, options2);

              that.setState({total_apps_1: total_apps});

              // EVENTS
              network2.on("click", function (params) {
                  console.log('params returns: ' + params.nodes);
                  if (params != null) {
                    if ( (params.nodes[0] != null) && (params.nodes[0].startsWith("app")) ) {
                      console.log('Showing node (application) information ...');
                      that.setState({ sel_service_instance_1: params.nodes[0].substr(3) });
                      if (that.state.sel_service_instance_1 != "") {
                        console.log("Looking for app [" + that.state.sel_service_instance_1 + "] ...");
                        that.getApp();
                      } else {
                        that.setState({ start_si_button: false, report_si_button: false, cancel_si_button: false });
                      }
                    }
                  }
              });
            }
            ////////////////////////////////////////////////////////////////////////////
          }
          else {
            that.setState({ show_alert: true, msg: "GET /api/v2/lm/service-instances/all", msg_content: JSON.stringify(body) + " => " + resp.statusCode });
          }
        }

        that.setState({isLoading: false});
      });
    }
    catch(err) {
      console.error(err);
      this.setState({ show_alert: true, msg: "GET /api/v2/lm/service-instances/all", msg_content: err.toString(), isLoading: false });
    }
  }



  /**
   * Render elements
   */
  render() {
    return (
      <div style={{margin: "0px 0px 0px 0px"}}>
        <form>
          <div className="form-group row">
            <div className="col-sm-7">
              <i class="fa fa-cogs" aria-hidden="true"></i>
              &nbsp;
              <b>Applications</b> managed by Edge-To-Cloud-Orchestration tool <Badge variant="secondary">{this.state.total_apps_1}</Badge>
              {this.state.isLoading ?
              <Spinner animation="border" role="status" variant="primary">
                <span className="sr-only">Loading...</span>
              </Spinner> : ""}
            </div>
            <div className="col-sm5">
            </div>
          </div>

          <div className="form-group row">
            <div className="col-sm-12">
              <div id="mynetwork3"></div>
            </div>
          </div>
          
          <div className="form-group row">
            <div className="col-sm-4" style={{margin: "0px 0px 0px 0px"}}>
              <Button variant="outline-primary" onClick={this.handleView2} size="sm">
                <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Refresh
              </Button>
              &nbsp;
              <Button variant="outline-danger" onClick={this.handleDelete} 
                    hidden={!this.state.notCreatingNew || this.state.isLoading} size="sm" disabled>
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Terminate
              </Button>
            </div>

            <div className="col-sm-6" style={{margin: "0px 0px 0px 0px"}}>
              <Dropdown size="sm" onSelect={this.handleSelectApp}>
                <Dropdown.Toggle variant="light" id="dropdown-basic" size="sm">Select application</Dropdown.Toggle>
                <Dropdown.Menu>
                  {this.state.apps.map(
                    (app) => (
                      <Dropdown.Item eventKey={app.id}>{app.name} - {app.id}</Dropdown.Item>
                    ),
                  )}
                </Dropdown.Menu>
              </Dropdown>

              {this.state.isLoading ?
                <Spinner animation="border" role="status" variant="primary">
                  <span className="sr-only">Loading...</span>
                </Spinner> : ""}
            </div>
          </div>  

          <div className="form-group row">
            <div className="col-sm-12" style={{margin: "0px 0px 0px 0px"}}>
              <Form style={{margin: "10px 0px 0px 15px"}}>
                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-1">Id</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="identifier" value={this.state.app_id}
                     style={{ backgroundColor: "#FFFFFC" }} disabled/>
                  </Col>

                  <Form.Text size="sm" column  className="col-sm-1">Name</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="name" value={this.state.app_name}
                     style={{ backgroundColor: "#FFFFFC" }} disabled/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-1">Description</Form.Text>
                  <Col sm={9}>
                    <Form.Control size="sm" placeholder="description" value={this.state.app_description}
                     style={{ backgroundColor: "#EEEEEE" }} disabled/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-1">Image</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="application image" value={this.state.app_image}
                     style={{ backgroundColor: "#EEEEEE" }} disabled/>
                  </Col>

                  <Form.Text size="sm" column  className="col-sm-1">Ports</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="ports used by the application" value={this.state.app_ports}
                     style={{ backgroundColor: "#EEEEEE"}} disabled/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-1">Type</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="application type" value={this.state.app_type}
                     style={{ backgroundColor: "#EEEEEE" }} disabled/>
                  </Col>

                  <Form.Text size="sm" column  className="col-sm-1">Replicas</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="number of replicas" value={this.state.app_replicas}
                     style={{ backgroundColor: "#EEEEEE"}} disabled/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column  className="col-sm-1">Cluster</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="cluster" value={this.state.app_cluster}
                     style={{ backgroundColor: "#EEEEEE" }} disabled/>
                  </Col>

                  <Form.Text size="sm" column className="col-sm-1">Created</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="creation date" value={this.state.app_created}
                     style={{ backgroundColor: "#FFFFFC" }} disabled/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column className="col-sm-1">Status</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="status" value={this.state.app_status}
                     style={{ backgroundColor: "#FFFFFC" }} disabled/>
                  </Col>

                  <Form.Text size="sm" column className="col-sm-1">Namespace</Form.Text>
                  <Col sm={4}>
                    <Form.Control size="sm" placeholder="Namespace" value={this.state.app_namespace}
                     style={{ backgroundColor: "#FFFFFC" }} disabled/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Form.Text size="sm" column className="col-sm-1">QoS</Form.Text>
                  <Col sm={9}>
                    <textarea className="form-control" id="job" rows="3" value={this.state.app_qos}>
                    </textarea>
                  </Col>
                </Form.Group>

              </Form>
            </div>
          </div>


          <Alert variant="danger" toggle={this.onDismiss} show={this.state.show_alert}>
            <p><b>{this.state.msg}</b></p>
            <p className="mb-0">{this.state.msg_content}</p>
            <div className="d-flex justify-content-end">
              <Button onClick={() => this.setState({ show_alert: false })} variant="outline-danger">
                Close
              </Button>
            </div>
          </Alert>

          <Alert variant="primary" toggle={this.onDismiss} show={this.state.show_info}>
            <p><b>{this.state.msg}</b></p>
            <p className="mb-0">{this.state.msg_content}</p>
            <div className="d-flex justify-content-end">
              <Button onClick={() => this.setState({ show_info: false })} variant="outline-primary">
                Close
              </Button>
            </div>
          </Alert>

        </form>
      </div>
    );
  }
}

export default Apps;
