//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package common

import (
	"encoding/json"
	"strings"
	"fmt"
	"strconv"
	"errors"

	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"

	"gopkg.in/yaml.v2"
)

// path used in logs
const pathLOG string = "E2CO > Common "

/*
StructToString Parses a struct to a string
*/
func StructToString(ct interface{}) (string, error) {
	out, err := json.Marshal(ct)
	if err != nil {
		log.Error(pathLOG+"[StructToString] ERROR ", err)
		return "", err
	}

	return string(out), nil
}

/*
AuthTokenNeeded Checks if Auth Token is needed to access the Orchestrator REST API
*/
func AuthTokenNeeded(orch structs.E2cOrchestrator) bool {
	if len(strings.TrimSpace(orch.ConnectionToken)) > 0 {
		return true
	}
	return false
}

/* 
GetAppNamespace get namespace where app is deployed
*/ 
func GetAppNamespace(app structs.E2coSubApp, orchFrom structs.E2cOrchestrator) string {
	for _, location := range app.Locations {
		if location.IDE2cOrchestrator == orchFrom.ID {
			return location.NameSpace
		}
	}

	return orchFrom.DefaultNamespace
}

/*
GetStringValueFromNumber
*/ 
func GetStringValueFromNumber(param interface{}) string {
	switch param.(type) { 
    default:
        return ""
    case uint64:
        return fmt.Sprintf("%d", param.(uint64))
	case int32:
        return fmt.Sprintf("%d", param.(int32))
	case int64:
        return fmt.Sprintf("%d", param.(int64))
	case int:
        return fmt.Sprintf("%d", param.(int))
	case float64:
		fIdStr := strconv.FormatFloat(param.(float64), 'f', -1, 64)
		return fIdStr
    case string:
        return param.(string)
    } 
}

/*
ParseYAMLObj
*/
func ParseYAMLObj(txt string, obj interface{}) error {
	err := yaml.Unmarshal([]byte(txt), obj)
	if err != nil {
		log.Error(pathLOG+"[ParseYAMLObj] Error unmarshalling YAML/JSON object: ", err)
		if e, ok := err.(*json.SyntaxError); ok {
			log.Info("[ParseYAMLObj] syntax error at byte offset ", e.Offset)
		}
		return errors.New("Error unmarshalling YAML/JSON object")
	}
	log.Trace(pathLOG+"[ParseYAMLObj] obj: \n", obj)

	return nil
}

/*
ParseJSONObj
*/
func ParseJSONObj(txt string, obj interface{}) error {
	return ParseBytesToJSONObj([]byte(txt), obj)
}

/*
ParseBytesToJSONObj
*/
func ParseBytesToJSONObj(b []byte, obj interface{}) error {
	err := json.Unmarshal(b, obj)
	if err != nil { // || obj == nil {
		log.Error(pathLOG+"[ParseBytesToJSONObj] Error unmarshalling JSON object: ", err)
		return errors.New("Error unmarshalling JSON object")
	}
	log.Trace(pathLOG+"[ParseBytesToJSONObj] obj: \n", obj)

	return nil
}