//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 07 Apr 2021
// Updated on 07 Apr 2021
//
// @author: ATOS
//
package http

import (
	"strconv"
	"sync"
	"net"
	"time"
	"errors"

	"atos.pledger/e2c-orchestrator/common/globals"
	"atos.pledger/e2c-orchestrator/data/structs"
	log "atos.pledger/e2c-orchestrator/common/logs"
)

/**
 *	Ports management: 
 *
 *	Management of the ports used by E2CO to expose applications
 */

// port number used to map compss applications
var (
	mu    sync.Mutex // guards balance
	rport int
)

// init
func init() {
	mu.Lock()
	rport =globals.PORT_INI
	mu.Unlock()
}

// newRPort: returns a new Port numberm and increments rport value
func newRPort() int {
	mu.Lock()
	log.Debug(pathLOG + "[NewRPort] Generating new PORT number ...")
	rport = rport + 1
	b := rport
	log.Debug(pathLOG + "[NewRPort] PORT = " + strconv.Itoa(b))
	mu.Unlock()
	return b
}

// checkPortAvailability: check port availability in a given URL
func checkPortAvailability(serviceURL string) bool {
	log.Debug(pathLOG + "[checkPortAvailability] Checking port availability [" + serviceURL + "] ...")

	timeout := 2 * time.Second
	_, err := net.DialTimeout("tcp", serviceURL, timeout) // serviceURL example: "192.168.1.133:4141"
	if err != nil {
		log.Info(pathLOG + "[checkPortAvailability] Destination Host [" + serviceURL + "] Unreachable. Port may be free")
		return true
	} 

	log.Info(pathLOG + "[checkPortAvailability] Port [" + serviceURL + "] is being used. Returning port ...")
	return false
}

/*
GetNewAvailablePort get new port to expose application
*/
func GetNewAvailablePort(orch structs.E2cOrchestrator) (int, error) {
	for i:=0;i<100;i++ {
		p := newRPort()
		serviceURL := orch.IP + ":" + strconv.Itoa(p)

		if checkPortAvailability(serviceURL) {
			return p, nil
		}
	}

	return -1, errors.New("After 100 tries, no available port was found in the following URL: " + orch.IP)
}