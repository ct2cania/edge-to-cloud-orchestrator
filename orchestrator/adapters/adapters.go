//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 18 Mar 2021
// Updated on 18 Mar 2021
//
// @author: ATOS
//
package adapters

import (
	"strings"

	"atos.pledger/e2c-orchestrator/common/globals"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"

	// deployment engine
	deploymentengine "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine"
	composeDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/compose"
	dockerDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/docker"
	k3sDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/k3s"
	k8sDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/k8s"
	microk8sDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/microk8s"
	openshiftDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/openshift"
	swarmDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/swarm"
	vmDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/vm"
	soeDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/soe"

	// Scaling Engine
	scalingengine "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine"
	composeScalEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/compose"
	dockerScalEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/docker"
	k3sScalEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/k3s"
	k8sScalEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/k8s"
	microk8sScalEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/microk8s"
	openshiftScalEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/openshift"
	swarmScalEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/swarm"
	vmScalEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/vm"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Adapters "

/*
GetDeploymentengineAdapter
*/
func GetDeploymentengineAdapter(t string) deploymentengine.Adapter {
	if strings.ToLower(t) == globals.KUBERNETES {
		return k8sDeplEng.Adapter{}
	} else if strings.ToLower(t) == globals.MICROK8S {
		return microk8sDeplEng.Adapter{}
	} else if strings.ToLower(t) == globals.K3S {
		return k3sDeplEng.Adapter{}
	} else if strings.ToLower(t) == globals.DOCKER {
		return dockerDeplEng.Adapter{}
	} else if strings.ToLower(t) == globals.OPENSHIFT {
		return openshiftDeplEng.Adapter{}
	} else if strings.ToLower(t) == globals.VM {
		return vmDeplEng.Adapter{}
	} else if strings.ToLower(t) == globals.DOCKERSWARM {
		return swarmDeplEng.Adapter{}
	} else if strings.ToLower(t) == globals.DOCKERCOMPOSE {
		return composeDeplEng.Adapter{}
	} else if strings.ToLower(t) == globals.SOE {
		return soeDeplEng.Adapter{}
	} 

	log.Warn(pathLOG + "[GetDeploymentengineAdapter] Adapter Not Supported: " + t)
	return NotImplementedAdapter{}
}

/*
GetScalingengineAdapter
*/
func GetScalingengineAdapter(t string) scalingengine.Adapter {
	if strings.ToLower(t) == globals.KUBERNETES {
		return k8sScalEng.Adapter{}
	} else if strings.ToLower(t) == globals.MICROK8S {
		return microk8sScalEng.Adapter{}
	} else if strings.ToLower(t) == globals.K3S {
		return k3sScalEng.Adapter{}
	} else if strings.ToLower(t) == globals.DOCKER {
		return dockerScalEng.Adapter{}
	} else if strings.ToLower(t) == globals.OPENSHIFT {
		return openshiftScalEng.Adapter{}
	} else if strings.ToLower(t) == globals.VM {
		return vmScalEng.Adapter{}
	} else if strings.ToLower(t) == globals.DOCKERSWARM {
		return swarmScalEng.Adapter{}
	} else if strings.ToLower(t) == globals.DOCKERCOMPOSE {
		return composeScalEng.Adapter{}
	}

	log.Warn(pathLOG + "[GetScalingengineAdapter] Adapter Not Supported: " + t)
	return NotImplementedAdapter{}
}

/*
NotImplementedDeploymentAdapter Adapter
*/
type NotImplementedAdapter struct{}

/*
DeployTask Deploy a task
*/
func (a NotImplementedAdapter) DeployTask(task *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Warn(pathLOG + "[DeployTask] Adapter Not Implemented")
	return "", nil
}

/*
RemoveTask Remove a task
*/
func (a NotImplementedAdapter) RemoveTask(task *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Warn(pathLOG + "[RemoveTask] Adapter Not Implemented")
	return "", nil
}

/*
GetTask Gets a task information (cluster information)
*/
func (a NotImplementedAdapter) GetTask(task structs.E2coSubApp, orch structs.E2cOrchestrator) (structs.E2coTaskStatusInfo, error) {
	log.Warn(pathLOG + "[GetTask] Adapter Not Implemented")
	return structs.E2coTaskStatusInfo{}, nil
}

/*
ScaleOut Scale number of replicas
*/
func (a NotImplementedAdapter) ScaleOut(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Warn(pathLOG + "[ScaleOut] Adapter Not Implemented")
	return "", nil
}

/*
ScaleIn Scale number of replicas
*/
func (a NotImplementedAdapter) ScaleIn(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Warn(pathLOG + "[ScaleIn] Adapter Not Implemented")
	return "", nil
}

/*
ScaleUp Scale up application
*/
func (a NotImplementedAdapter) ScaleUp(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Warn(pathLOG + "[ScaleUp] Adapter Not Implemented")
	return "", nil
}

/*
ScaleDown Scale down applicatoin
*/
func (a NotImplementedAdapter) ScaleDown(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Warn(pathLOG + "[ScaleDown] Adapter Not Implemented")
	return "", nil
}
