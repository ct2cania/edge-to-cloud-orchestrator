//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 23 Mar 2021
// Updated on 18 Mar 2021
//
// @author: ATOS
//
package apps

import (
	deploymentengine "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine"
	log "atos.pledger/e2c-orchestrator/common/logs"
	structs "atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/data/db"
)


/*
infoProcess info processes
*/
func infoProcess(app *structs.E2coApplication, sapp structs.E2coSubApp, orch structs.E2cOrchestrator, a deploymentengine.Adapter) error {
	log.Debug(pathLOG + "[infoProcess] Starting info process ...")

	// get application info
	status, err := a.GetTask(sapp, orch) // structs.E2coTaskStatusInfo, error
	if err != nil {
		log.Error(pathLOG+"[infoProcess] Error getting info from application: ", err)
		return err
	} else {
		log.Debug(pathLOG + "[infoProcess] Application info gathered. Updating application ...")
		sapp.Info = status

		// update subapp
		err = db.UpdateE2coSubApp(sapp.ID, sapp)
		if err != nil {
			log.Error(pathLOG+"[infoProcess] Error updating application info status in database: ", err)
		}

		// update app
		for i, sa := range app.Apps {
			if sa.ID == sapp.ID {
				app.Apps[i].Info = status
				break
			}
		}
	}

	return nil
}