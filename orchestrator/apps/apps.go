//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 24 Sep 2021
// Updated on 24 Sep 2021
//
// @author: ATOS
//
package apps

import (
	"errors"
	"strconv"
	"time"

	"atos.pledger/e2c-orchestrator/common/globals"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/db"
	"atos.pledger/e2c-orchestrator/data/structs"
	migrationengine "atos.pledger/e2c-orchestrator/orchestrator/apps/migration-engine"
	deploymentengine "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine"
	"atos.pledger/e2c-orchestrator/orchestrator/adapters"

	"github.com/spf13/viper"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Applications "

//	Warming control
var operationHistory map[string]int64
var WARMING_TIME int = -1 // seconds

// getDeploymentInfo Gets the orchestrator and adapted of the application
func getDeploymentInfo(id string) (*structs.E2coSubApp, deploymentengine.Adapter, *structs.E2cOrchestrator, error) {
	// get orchestrator to get adapter and endpoint
	orch, err := db.ReadOrchestrator(id)
	if err != nil {
		log.Error(pathLOG+"[getDeploymentInfo] Error reading orchestrator from database: ", err)
		return nil, nil, nil, errors.New("Error reading orchestrator from database: " + err.Error())
	}

	// get adapter
	a := adapters.GetDeploymentengineAdapter(orch.Type)

	return nil, a, &orch, nil	
}


// checkWarmingTimeCompleted
func checkWarmingTimeCompleted(appID string) bool {
	var result bool = false
	log.Info(pathLOG + "Checking Warming time for app " + appID)
	if operationHistory == nil {
		operationHistory = make(map[string]int64)
	}
	if WARMING_TIME == -1 {
		var config = viper.New()
		config.SetEnvPrefix("E2CO")
		config.AutomaticEnv()
		n, err := strconv.Atoi(config.GetString("WARMING_TIME"))
		if err != nil {
			WARMING_TIME = 60 // seconds
		} else {
			WARMING_TIME = n
		}
	}
	now := time.Now()
	if operationHistory[appID] == 0 {
		operationHistory[appID] = now.Unix()
		result = true
	} else {
		if operationHistory[appID]+int64(WARMING_TIME) > now.Unix() {
			result = false
		} else {
			operationHistory[appID] = now.Unix()
			result = true
		}
	}
	return result
}

///////////////////////////////////////////////////////////////////////////////
// ORCHESTRATORS

/*
AddNewOrchestrator Adds a new orchestrator
*/
func AddNewOrchestrator(orch structs.E2cOrchestrator) (structs.E2cOrchestrator, error) {
	log.Info(pathLOG + "[AddNewOrchestrator] Adding new orchestrator ...")

	// get value to check if already exists
	dbOrch, err := db.ReadOrchestrator(orch.ID)
	if err == nil {
		log.Warn("[UpdateNewOrchestrator] Orchestrator already exists (same ID)")
		return dbOrch, errors.New("Orchestrator already exists (same ID)")
	} else {
		log.Debug("[UpdateNewOrchestrator] Orchestrator not found in DB. It can be added to DB.")
		
		// default values
		if orch.DefaultNamespace == "" {
			orch.DefaultNamespace = globals.ORCHESTRATOR_DEFAULT_NAMESPACE
		}

		// save to DB
		err = db.SetOrchestrator(orch.ID, orch)
		if err != nil {
			log.Error("Error storing value in database: ", err)
			return dbOrch, err
		}

		// get value
		dbOrch, err = db.ReadOrchestrator(orch.ID)
		if err != nil {
			log.Error("Error retrieving value from database: ", err)
		}
	}

	return dbOrch, nil
}

/*
UpdateOrchestrator Updates an orchestrator
*/
func UpdateOrchestrator(idOrch string, orch structs.E2cOrchestrator) (structs.E2cOrchestrator, error) {
	log.Info(pathLOG + "[UpdateOrchestrator] Updating orchestrator [" + idOrch + "] ...")

	// get value to check if already exists
	_, err := db.ReadOrchestrator(idOrch)
	if err != nil {
		log.Warn("[UpdateOrchestrator] Orchestrator doesn't exist")
		return orch, err
	}

	// save to DB
	err = db.UpdateOrchestrator(orch.ID, orch)
	if err != nil {
		log.Error("[UpdateOrchestrator] Error updating value in database: ", err)
		return orch, err
	}

	// get value
	dbOrch, err := db.ReadOrchestrator(orch.ID)
	if err != nil {
		log.Error("[UpdateOrchestrator] Error retrieving value from database: ", err)
		return orch, nil
	} else {
		return dbOrch, nil
	}
}

/*
RemoveOrchestrator Removes a orchestrator
*/
func RemoveOrchestrator(idOrch string) error {
	log.Info(pathLOG + "[RemoveOrchestrator] Deleting orchestrator [" + idOrch + "] ...")

	// check if orchestrator exists
	_, err := db.ReadOrchestrator(idOrch)
	if err != nil {
		log.Error("[RemoveOrchestrator] Error retrieving value from database: ", err)
		return err
	}

	_, err = db.DeleteOrchestrator(idOrch)
	if err != nil {
		log.Error("[RemoveOrchestrator] Error deleting orchestrator: ", err)
		return err
	}
	return nil
}

///////////////////////////////////////////////////////////////////////////////
// APPLICATIONS

/*
DeployE2coApplication Deploy a E2coApplication
*/
func DeployE2coApplication(application *structs.E2coApplication, isNew bool) error {
	if isNew {
		log.Info(pathLOG + "[DeployE2coApplication] Deploying [new] application ...")
	} else {
		log.Info(pathLOG + "[DeployE2coApplication] Deploying [stored / paused] application ...")
	}

	// background process - INI
	go func(app *structs.E2coApplication) {
		errDeploymentStr := ""

		for index, app := range application.Apps {
			log.Info(pathLOG + "[DeployE2coApplication] application [" + app.Name + ", " + app.ID + "] ...")

			for _, location := range app.Locations {
				log.Info(pathLOG + "[DeployE2coApplication] Deploying application.Apps[" + strconv.Itoa(index) + ", " + app.Name + "] in location [" + location.IDE2cOrchestrator + "] ...")
				err := deploySubApp(&app, location)
				if err != nil {
					errDeploymentStr = errDeploymentStr + err.Error() + " /n"
					subAppDeploymentError(&app, location);
				}
			}
		}

		if len(errDeploymentStr) > 0 {
			log.Error(pathLOG + "[DeployE2coApplication] Errors during the deployment of the application: " + errDeploymentStr)
			application.Status = globals.DEPLOYMENT_ERROR //"Error"
			application.Info.Status = globals.DEPLOYMENT_ERROR //"Deployment_Error"
			application.Info.Message = "Errors during the deployment of the application: " + errDeploymentStr
		} else {
			application.Status = globals.DEPLOYMENT_DEPLOYED //"Deployed"
			application.Info.Status = globals.DEPLOYMENT_DEPLOYED //"Deployed"
			application.Info.Message = "Application deployed. ID: " + application.ID
		}

		// wait 5 seconds before updating value
		time.Sleep(5 * time.Second)
		// store E2coApplication in DB
		db.UpdateE2coApplication(application.ID, *application) 
	}(application)
	// background process - END

	if application.DryRun {
		application.Status = globals.DEPLOYMENT_STORED_PAUSED
		application.Info.Status = globals.DEPLOYMENT_STORED_PAUSED
		application.Info.Message = "Application stored/paused. ID: " + application.ID
	} else {
		application.Status = globals.DEPLOYMENT_DEPLOYING //"Deploying"
		application.Info.Status = globals.DEPLOYMENT_DEPLOYING //"Deploying_Application"
		application.Info.Message = "Application is being deployed. ID: " + application.ID
	}
	
	if isNew { // store E2coApplication in DB if application is new (==> REST API services)
		db.SetE2coApplication(application.ID, *application) 
	} else {  // update E2coApplication if application is not new (==> Eventhadler / Kafka)
		db.UpdateE2coApplication(application.ID, *application) 
	}
	
	return nil // TODO handle errors
}

/*
RemoveE2coApplication Deploy a E2coApplication
*/
func RemoveE2coApplication(id string) error {
	log.Info(pathLOG + "[RemoveE2coApplication] Removing application [" + id + "] ...")

	// get application from database
	application, err := db.ReadE2coApplication(id) // response: ([]structs.E2coApplication, error)
	if err != nil {
		log.Error(pathLOG+"[RemoveE2coApplication] Error reading application from database: ", err)
		return errors.New("Error reading application from database: " + err.Error())
	}

	// save task in DB; save status to DEPLOYMENT_TERMINATING
	application.Status = globals.DEPLOYMENT_TERMINATING
	err = db.UpdateE2coApplication(id, application)
	if err != nil {
		log.Error(pathLOG+"[RemoveE2coApplication] Error saving application status in database: ", err)
	}

	// remove each subapp ([]task.Apps) from correspondent location ([]task.Locations / []task.App.Locations)
	for index, s := range application.Apps {

		subapp, err := db.ReadE2coSubApp(s.ID)
		if err != nil {
			log.Error(pathLOG+"[RemoveE2coApplication] Error getting complete information from subapplication: ", err)
		}

		for _, location := range subapp.Locations {
			log.Info(pathLOG + "[RemoveE2coApplication] Removing application.Apps[" + strconv.Itoa(index) + ", " + subapp.Name + "] from location [" + location.IDE2cOrchestrator + "] ...")

			// background process
			go func(sapp *structs.E2coSubApp, location structs.E2coLocation) {
				// get app, adapter and orchestrator
				_, a, orch, err := getDeploymentInfo(location.IDE2cOrchestrator)
				if err != nil {
					log.Error(pathLOG+"[RemoveE2coApplication] Error getting information from orchestrator: ", err)
					log.Error(pathLOG+"[RemoveE2coApplication] Sub application was not removed from orchestrator")
				} else {
					// background termination process
					terminationProcess(*sapp, *orch, a)
				}

				// delete sub app from DB
				time.Sleep(1 * time.Second)
				log.Info(pathLOG + "[RemoveE2coApplication] Removing sub application [" + sapp.ID + "] from database ...")
				_, err = db.DeleteE2coSubApp(sapp.ID)
				if err != nil {
					log.Error(pathLOG+"[RemoveE2coApplication] Error removing sub application [" + sapp.ID + "] from database: ", err)
				}
			}(&subapp, location)
		}
	}

	// delte app from DB
	log.Info(pathLOG + "[RemoveE2coApplication] Removing application [" + id + "] from database ...")
	_, err = db.DeleteE2coApplication(id)
	if err != nil {
		log.Error(pathLOG+"[RemoveE2coApplication] Error removing application [" + id + "] from database: ", err)
	}

	return nil
}

/*
GetE2coApplication get cluster's E2coApplication information
*/
func GetE2coApplication(idTask string) (structs.E2coApplication, error) {
	log.Info(pathLOG + "[GetE2coApplication] Getting cluster's task [" + idTask + "] ...")

	// get application from database
	application, err := db.ReadE2coApplication(idTask)
	if err != nil {
		log.Error(pathLOG+"[GetE2coApplication] Error (1) getting application: ", err)
		return structs.E2coApplication{}, err
	}

	// update each subapp ([]task.Apps) status from correspondent locations ([]task.Locations / []task.App.Locations)
	for index, s := range application.Apps {
		subapp, err := db.ReadE2coSubApp(s.ID)
		if err != nil {
			log.Error(pathLOG+"[GetE2coApplication] Error (2) getting complete information from subapplication: ", err)
		}

		for _, location := range subapp.Locations {
			log.Info(pathLOG + "[GetE2coApplication] Getting information of application.Apps[" + strconv.Itoa(index) + ", " + subapp.Name + "] located in [" + location.IDE2cOrchestrator + "] ...")
			// get app, adapter and orchestrator
			_, a, orch, err := getDeploymentInfo(location.IDE2cOrchestrator)

			if err != nil {
				log.Error(pathLOG+"[GetE2coApplication] Error (3) getting information from orchestrator: ", err)
				log.Error(pathLOG+"[GetE2coApplication] Sub application was not removed from orchestrator")
			} else {
				// background info gathering process
				infoProcess(&application, subapp, *orch, a) // (structs.E2coTaskStatusInfo, error)


			}
		}
	}

	// update app
	err = db.UpdateE2coApplication(idTask, application)
	if err != nil {
		log.Error(pathLOG+"[GetE2coApplication] Error updating application info status in database: ", err)
	}

	// get updated E2coApplication from DB
	resp, err := db.ReadE2coApplication(idTask)
	if err != nil {
		log.Error(pathLOG+"[GetE2coApplication] Error (4) getting application: ", err)
		return structs.E2coApplication{}, err
	}

	return resp, nil
}

/*
GetE2coApp get cluster's E2coApplication information
*/
func GetE2coApp(id string) (structs.E2coSubApp, error) {
	log.Info(pathLOG + "[GetE2coApp] Getting sub application [" + id + "] ...")

	// get E2coApplication from DB
	resp, err := db.ReadE2coSubApp(id)
	if err != nil {
		log.Error(pathLOG+"[GetE2coApp] Error getting sub application: ", err)
		return structs.E2coSubApp{}, err
	}

	return resp, nil
}

/*
UpdateE2coApplication Updates a E2coApplication
*/
func UpdateE2coApplication(app *structs.E2coApplication, operation structs.TaskOperation) error {
	log.Info(pathLOG + "[UpdateE2coApp] Updating application [" + app.ID + "] [" + operation.Operation + "] ...")

	for _, s := range app.Apps {
		subapp, err := db.ReadE2coSubApp(s.ID)
		if err != nil {
			log.Error(pathLOG+"[UpdateE2coApp] Error getting complete information from subapplication: ", err)
		}

		UpdateE2coSubApp(&subapp, operation)
	}

	return nil
}

/*
UpdateE2coSubApp Updates a E2coSubApp
*/
func UpdateE2coSubApp(app *structs.E2coSubApp, operation structs.TaskOperation) error {
	log.Info(pathLOG + "[UpdateE2coSubApp] Updating subapplication [" + operation.Operation + "] ...")

	// get orchestrator where the application is deployed
	var orchFrom structs.E2cOrchestrator

	if len(app.Locations) > 0 {
		idOrch := app.Locations[0].IDE2cOrchestrator
		if len(operation.From) > 0 {
			idOrch = operation.From
		}
		o, err := db.ReadOrchestrator(idOrch)
		if err != nil {
			return errors.New("Error getting value from database: " + err.Error())
		}
		orchFrom = o
	} else {
		return errors.New("No orchestrator defined in E2CO subapplication")
	}

	// check WarmingTime before executing operation
	if checkWarmingTimeCompleted(app.ID) {
		// execute operation
		if operation.Operation == "update" {				
			// UPDATE Subapplication
			// TODO
		} else if operation.Operation == globals.MIGRATION {		
			// MIGRATE Subapplication
			migrationengine.MigrateApp(app, orchFrom, operation)
		} else {											// SCALE UP/DOWN/IN/OUT
			// call to adapter
			a := adapters.GetScalingengineAdapter(orchFrom.Type)

			// switch scaling operation
			switch op := operation.Operation; op {
				case globals.SCALE_OUT:
					if operation.Replicas > 0 {
						app.Replicas = operation.Replicas
						if app.MaxReplicas == 0 || app.MaxReplicas >= operation.Replicas {
							a.ScaleOut(app, orchFrom, operation)
						} else {
							//if task.MaxReplicas < operation.Replicas {
							operation.Replicas = app.MaxReplicas
							a.ScaleOut(app, orchFrom, operation)
							//}
							//log.Info("Max replicas of this app reached " + task.ID)
						}
					}
				case globals.SCALE_IN:
					if operation.Replicas > 0 {
						a.ScaleIn(app, orchFrom, operation)
					}
				case globals.SCALE_UP:
					a.ScaleUp(app, orchFrom, operation)
				case globals.SCALE_DOWN:
					a.ScaleDown(app, orchFrom, operation)
				default:
			}

			// save task to DB
			err := db.SetE2coSubApp(app.ID, *app) // (id string, dbtask structs.E2coTaskExtended) returns (error)
			if err != nil {
				log.Error("[UpdateE2coSubApp] Error storing value in database: ", err)
				return errors.New("Error updating value in database: " + err.Error())
			}
		}
	} else {
		log.Info(pathLOG + "Warming time is not completed for this app: " + app.ID)
	}

	return nil
}

/*
 */
 func ResetDryRun(task *structs.E2coApplication) {
	//task.DryRun = false
	app, _, _, err := getDeploymentInfo2(task.ID, true)
	if err != nil {
		app = task
	}
	app.DryRun = false
	err = db.UpdateE2coApplication(task.ID, *app) // (id string, dbtask structs.E2coTaskExtended) returns (error)
	if err != nil {
		log.Error("[ResetDryRun] Error storing value in database: ", err)
		//return errors.New("Error updating value in database: " + err.Error())
	}
}

/*
getDeploymentInfo Gets the orchestrator and adapted of the application
*/
func getDeploymentInfo2(id string, lookForTask bool) (*structs.E2coApplication, deploymentengine.Adapter, *structs.E2cOrchestrator, error) {
	if lookForTask {
		// get app from database
		app, err := db.ReadE2coApplication(id) // response: ([]structs.E2coTaskExtended, error)
		if err != nil {
			log.Error(pathLOG+"[getDeploymentInfo] Error reading application from database: ", err)
			return nil, nil, nil, errors.New("Error reading application from database: " + err.Error())
		}

		// get orchestrator to get adapter and endpoint
		orch, err := db.ReadOrchestrator(app.Locations[0].IDE2cOrchestrator)
		if err != nil {
			log.Error(pathLOG+"[getDeploymentInfo] Error reading orchestrator from database: ", err)
			return nil, nil, nil, errors.New("Error reading orchestrator from database: " + err.Error())
		}

		// get adapter
		a := adapters.GetDeploymentengineAdapter(orch.Type)

		return &app, a, &orch, nil
	} else {
		// get orchestrator to get adapter and endpoint
		orch, err := db.ReadOrchestrator(id)
		if err != nil {
			log.Error(pathLOG+"[getDeploymentInfo] Error reading orchestrator from database: ", err)
			return nil, nil, nil, errors.New("Error reading orchestrator from database: " + err.Error())
		}

		// get adapter
		a := adapters.GetDeploymentengineAdapter(orch.Type)

		return nil, a, &orch, nil
	}	
}

