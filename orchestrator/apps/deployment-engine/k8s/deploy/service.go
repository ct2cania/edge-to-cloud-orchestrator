//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 07 Apr 2021
//
// @author: ATOS
//
package deploy

import (
	"strconv"
	"strings"

	"atos.pledger/e2c-orchestrator/common"
	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
)

/* K8S SERVICE example:

{
  "apiVersion": "v1",
  "kind": "Service",
  "metadata": {
    "name": "my-service"
  },
  "spec": {
    "selector": {
      "app": "MyApp"
    },
    "ports": [
      {
        "name": "http",
        "protocol": "TCP",
        "port": 80,
        "targetPort": 9376
      },
      {
        "name": "https",
        "protocol": "TCP",
        "port": 443,
        "targetPort": 9377
      }
    ]
  }
}

*/

/* 
NewServiceStruct creates a new K8s Service json
*/
func NewServiceStruct(app *structs.E2coSubApp, namespace string, ip string) (*structs.K8sService, int, string) {
	jsonService, mainPort, mainPortName := serviceStruct(app, namespace)

	var ips = []string{ip}
	jsonService.Spec.ExternalIPs = ips

	return jsonService, mainPort, mainPortName
}

// serviceStruct: creates a new K8s Service json
func serviceStruct(app *structs.E2coSubApp, namespace string) (*structs.K8sService, int, string) {
	var jsonService *structs.K8sService
	jsonService = new(structs.K8sService)

	jsonService.APIVersion = "v1"
	jsonService.Kind = "Service"
	jsonService.Metadata.Name = "serv-" + app.ID
	jsonService.Metadata.Namespace = namespace
	jsonService.Metadata.Labels = map[string]interface{}{
		"app": app.ID,
	}
	jsonService.Spec.Selector = map[string]interface{}{
		"app": app.ID,
	}

	mainPort := 0
	mainPortName := ""
	// ports
	for _, contElement := range app.Containers {
		for _, portElement := range contElement.Ports {
			jsonService.Spec.Ports =
				append(jsonService.Spec.Ports, structs.K8sServicePort{
					Name:       strconv.Itoa(portElement.ContainerPort) + "-" + strings.ToLower(portElement.Protocol),
					Port:       32001, //portElement.ContainerPort,	// TODO service field
					Protocol:   strings.ToUpper(portElement.Protocol),
					TargetPort: portElement.ContainerPort})
			if mainPort == 0 {
				mainPort = portElement.ContainerPort
				mainPortName = strconv.Itoa(portElement.ContainerPort) + "-" + portElement.Protocol
			}
		}

		// metadata: labels
		totalLabels := len(contElement.Labels)
		if totalLabels > 0 {
			m := jsonService.Metadata.Labels
			m2 := jsonService.Spec.Selector
			for k := 0; k < totalLabels; k++ {
				m[contElement.Labels[k].Key] = contElement.Labels[k].Value
				m2[contElement.Labels[k].Key] = contElement.Labels[k].Value
			}
		}

	}

	return jsonService, mainPort, mainPortName
}

/* 
createK8sService creates the K8s service json; call to Kubernetes API to create a service
*/
func CreateK8sService(app *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool) (string, int, string, error) {
	log.Info(pathLOG + "[CreateK8sService] Generating 'service' json ...")

	namespace := common.GetAppNamespace(*app, orch)
	k8sServ, mainPort, mainPortName := NewServiceStruct(app, namespace, orch.IP) // returns *structs.K8sService

	strTxt, _ := common.StructToString(*k8sServ)
	log.Info(pathLOG + "[CreateK8sService] [" + strTxt + "]")

	// CALL to Kubernetes API to launch a new service
	log.Info(pathLOG + "[CreateK8sService] Creating a new service in K8s cluster ...")
	status, _, err := http.PostStruct(
		cfg.GetPathKubernetesCreateService(orch, namespace),
		sec,
		orch.ConnectionToken,
		k8sServ)
	if err != nil {
		log.Error(pathLOG+"[CreateK8sService] ERROR", err)
		return "", -1, "", err
	}
	log.Info(pathLOG + "[CreateK8sService] RESPONSE: OK")

	return strconv.Itoa(status), mainPort, mainPortName, nil
}