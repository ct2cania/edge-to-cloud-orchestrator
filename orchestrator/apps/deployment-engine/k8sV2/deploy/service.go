//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 01 Jul 2021
// Updated on 01 Jul 2021
//
// @author: ATOS
//
package deploy

import (
	"strconv"
	//"context"
	"strings"

	//"k8s.io/client-go/kubernetes"
	//"k8s.io/client-go/rest"
	//appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	corev1 "k8s.io/api/core/v1"

	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	//"atos.pledger/e2c-orchestrator/common"
)

/******************************************************************************
https://pkg.go.dev/k8s.io/api@v0.21.2/core/v1

SERVCICE:

	clientset.CoreV1().Services("kube-system").Create(&corev1.Service{
            ObjectMeta: metav1.ObjectMeta{
                Name:                       controllerSVCName,
                Namespace:                  "kube-system",
                Labels: map[string]string{
                    "k8s-app": "kube-controller-manager",
                },
            },
            Spec: corev1.ServiceSpec{
                Ports:                    nil,
                Selector:                 nil,
                ClusterIP:                "",

            },
   })


	https://pkg.go.dev/k8s.io/api@v0.21.2/core/v1#Service 

	type Service struct {
		metav1.TypeMeta `json:",inline"`
		metav1.ObjectMeta `json:"metadata,omitempty" protobuf:"bytes,1,opt,name=metadata"`
		Spec ServiceSpec `json:"spec,omitempty" protobuf:"bytes,2,opt,name=spec"`
		Status ServiceStatus `json:"status,omitempty" protobuf:"bytes,3,opt,name=status"`
	}


******************************************************************************/

// newServiceStruct: creates a new K8s Service json
func newServiceStruct(app *structs.E2coSubApp, ip string) (*corev1.Service, int, string) {
	service, mainPort, mainPortName := serviceStruct(app)

	if app.Service.Expose {
		var ips = []string{ip}
		service.Spec.ExternalIPs = ips
	}

	return service, mainPort, mainPortName
}

// serviceStruct: creates a new K8s Service json
func serviceStruct(app *structs.E2coSubApp) (*corev1.Service, int, string) {
	// labels
	labelsObjectMeta := map[string]string{}
	labelsSpecSelector := map[string]string{}

	labelsObjectMeta["app"] = app.ID
	labelsSpecSelector["app"] = app.ID

	totalLabels := len(app.Labels)
	if totalLabels > 0 {
		for k := 0; k < totalLabels; k++ {
			labelsObjectMeta[app.Labels[k].Key] = app.Labels[k].Value
			labelsSpecSelector[app.Labels[k].Key] = app.Labels[k].Value
		}
	}

	// ports
	totalContainers := len(app.Containers)
	for i := 0; i < totalContainers; i++ {
		// ports:
		ports := []apiv1.ContainerPort{}
		totalPorts := len(app.Containers[i].Ports)
		for j := 0; j < totalPorts; j++ {
			p := apiv1.ContainerPort{
				//Name:          "http",
				Protocol:      apiv1.ProtocolTCP,
				ContainerPort: int32(app.Containers[i].Ports[j].ContainerPort),
			}
			ports = append(ports, p)
		}
	}

	// service:
	service := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:       "serv-" + app.ID,
			Labels: 	labelsObjectMeta,
		},
		Spec: corev1.ServiceSpec{
			Ports:		nil,
			Selector: 	labelsSpecSelector,
			ClusterIP:  "",
		},
	}




	var jsonService *structs.K8sService
	jsonService = new(structs.K8sService)

	jsonService.APIVersion = "v1"
	jsonService.Kind = "Service"
	jsonService.Metadata.Name = "serv-" + app.ID
	jsonService.Metadata.Labels = map[string]interface{}{
		"app": app.ID,
	}
	jsonService.Spec.Selector = map[string]interface{}{
		"app": app.ID,
	}

	mainPort := 0
	mainPortName := ""
	// ports
	for _, contElement := range app.Containers {
		for _, portElement := range contElement.Ports {
			jsonService.Spec.Ports =
				append(jsonService.Spec.Ports, structs.K8sServicePort{
					Name:       strconv.Itoa(portElement.ContainerPort) + "-" + strings.ToLower(portElement.Protocol),
					Port:       32001, //portElement.ContainerPort,	// TODO service field
					Protocol:   strings.ToUpper(portElement.Protocol),
					TargetPort: portElement.ContainerPort})
			if mainPort == 0 {
				mainPort = portElement.ContainerPort
				mainPortName = strconv.Itoa(portElement.ContainerPort) + "-" + portElement.Protocol
			}
		}

		// metadata: labels
		totalLabels := len(contElement.Labels)
		if totalLabels > 0 {
			m := jsonService.Metadata.Labels
			m2 := jsonService.Spec.Selector
			for k := 0; k < totalLabels; k++ {
				m[contElement.Labels[k].Key] = contElement.Labels[k].Value
				m2[contElement.Labels[k].Key] = contElement.Labels[k].Value
			}
		}

	}

	return service, mainPort, mainPortName
}

/* 
createK8sService creates the K8s service json; call to Kubernetes API to create a service
*/
func CreateK8sService(app *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool) (string, int, string, error) {
	log.Info(pathLOG + "[CreateK8sService] Generating 'service' json ...")
	// k8sServ
	_, mainPort, mainPortName := newServiceStruct(app, orch.IP) // returns *corev1.Service
/*
	var cfg rest.Config
	// e.g. "https://192.168.1.141:16443"
	cfg.Host = orch.RESTAPIEndPoint
	// e.g. "eyJhbGciOiJSUzI1NiIsImtpZCI6IklxdWt0c01TY1R6TkdEWGsxcFlsT3BWcVlnVlJqT2U0VDNFcHd6UVVaZ1kifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLXh0cXR2Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJjY2NjZGE0OS02NGY1LTRkNDQtYWRjZi1kNjliM2E5YzRhMmEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06ZGVmYXVsdCJ9.PmBQyI-pN5mcSk-e20nnWKQ2jeR72WNjg_UD1mAfpWKBYPbyxs3MLYWVvwuj2zydYZsLkCvHQJSsrI5uMdPAZoc-2OPfV8pbRTGFDplX9us76R0WOv1_jCb0e6GhMI0jX4-OIXdcR2lC0VfQ5MfYcoVG16GMq6LWgw_Uxm4zGVqZ-QWw1CCg-QbkTTaoQ1h07w_2Dtp4QWKJr3evPj28XSn_PgwHzP-rWntOnMaMbRPMCyzri3Dq0ZAX4oZgAUWU87w99DexcX3rMPWhiu2E-hwmKkleJgbcn28xPDW2BHEncWUMUD38EIp3my9XUjd5H2awlmMIdn3NCoo1Jzdrtg"
	cfg.BearerToken = orch.ConnectionToken
	// Insecure = true
	cfg.TLSClientConfig.Insecure = true

	// create the clientset
	clientset, err := kubernetes.NewForConfig(&cfg)
	if err != nil {
		log.Error(pathLOG + "[CreateK8sService] Error creating the client connection: ", err)
		return "", -1, "", err
	}

	// Create Service
	serviceClient := clientset.CoreV1().Services(common.GetAppNamespace(*app, orch)) // apiv1.NamespaceDefault

	log.Info(pathLOG + "[CreateK8sService] Creating service ...")
	result, err := serviceClient.Create(context.TODO(), k8sServ, metav1.CreateOptions{})
	if err != nil {
		log.Error(pathLOG + "[CreateK8sService] Errorcreating application service: ", err)
		return "", -1, "", err
	}
	log.Info(pathLOG + "[CreateK8sService] Created service %q.", result.GetObjectMeta().GetName())
*/
	return "200", mainPort, mainPortName, nil
}