//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 07 Apr 2021
// Updated on 07 Apr 2021
//
// @author: ATOS
//
package k8sV2

import (
	"atos.pledger/e2c-orchestrator/common"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	appdeploy "atos.pledger/e2c-orchestrator/orchestrator/apps/deployment-engine/k8sV2/deploy"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Deployment-Engine > K8sV2 "

/*
Adapter K8s Adapter struct
*/
type Adapter struct{}

/*
GetDeploymentErrorResponse Error Response from call to K8s REST API
*/
type DeploymentErrorResponse struct {
	Code    int    `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

/*
DeploymentResponse Response from call to K8s REST API
*/
type DeploymentResponse struct {
	Status  DeploymentStatusResponse `json:"status,omitempty"`
	Code    int                      `json:"code,omitempty"`
	Message string                   `json:"message,omitempty"`
}

/*
DeploymentStatusResponse Status struct
*/
type DeploymentStatusResponse struct {
	Replicas          int `json:"replicas,omitempty"`
	ReadyReplicas     int `json:"readyReplicas,omitempty"`
	AvailableReplicas int `json:"availableReplicas,omitempty"`
}

///////////////////////////////////////////////////////////////////////////////

/*
GetApp get full deployment information; CALL to Kubernetes API to get deployment
*/
func (a Adapter) GetApp(app structs.E2coSubApp, orch structs.E2cOrchestrator) (structs.E2coTaskStatusInfo, error) {
	log.Info(pathLOG + "[GetApp] Getting deployment from K8s cluster ...")

	// structs.E2coTaskStatusInfo
	status := structs.E2coTaskStatusInfo{}

	return status, nil
}


/*
DeployApp Deploy a subapplication / service (k8s: deployment & service & volumes ...)
*/
func (a Adapter) DeployApp(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Debug(pathLOG + "[DeployApp] Deploying K8s components (deployment, service) for the new application ...")
	log.Debug(pathLOG + "[DeployApp] Orchestrator id = " + orch.ID + ", namespace = " + common.GetAppNamespace(*app, orch) + ", host = " + orch.IP + "")

	sec := common.AuthTokenNeeded(orch)	// Auth Token needed?
	status, err := appdeploy.CreateK8sDeployment(app, orch, sec)	// 1. DEPLOYMENT /////
	if err != nil {
		log.Error(pathLOG+"[DeployApp] ERROR (1)", err)
		return "", err
	} else if (status == "200" || status == "201") { // create service if Exposed == true
		if app.Service.Expose {
			status, _, _, err := appdeploy.CreateK8sService(app, orch, sec)	// 2. SERVICE /////
			if err != nil {
				log.Error(pathLOG+"[DeployApp] ERROR (2)", err)
				return "", err
			} else if status == "200" || status == "201" {
				log.Info(pathLOG + "[DeployApp] Application components (deployment, service) deployed with success")
				return "ok", nil
			}
		} else {
			log.Info(pathLOG + "[DeployApp] Application components (deployment) deployed with success. No service was created. ")
			return "ok", nil
		}
	}

	err = errors.New("Components creation failed. status = [" + status + "]")
	log.Error(pathLOG+"[DeployApp] ERROR (4)", err)
	return "", err

	return "", nil
}


/*
RemoveApp Removes a subapplication from orchestrator / service
*/
func (a Adapter) RemoveApp(app *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Info(pathLOG + "[RemoveApp] Deleting application ...")

	namespace := common.GetAppNamespace(*app, orch)
	name := app.ID
	log.Info(pathLOG + "[RemoveApp] Deleting task [" + name + "] from [" + namespace + "] ...")

	
	
	return "", nil
}