//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package docker

import (
	"context"
	"errors"
	"io"
	"os"
	"strconv"
	"strings"
	"math/rand"
	"time"
	"encoding/base64"
	"encoding/json"

	"atos.pledger/e2c-orchestrator/common/http"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
)

// createStartContainer: Creates and starts the container. Returns the ID of the container
func createStartContainer(cli client.Client, subapp *structs.E2coSubApp, orch structs.E2cOrchestrator) (string, error) {
	log.Info(pathLOG + "[createStartContainer] Creating and starting docker container...")

	// TODO multiple Containers
	// IMAGE e.g. "nginx"
	log.Debug(pathLOG + "[createStartContainer] Setting image ...")
	image := subapp.Containers[0].Image

	// PORTs (application ports) e.g. "80/tcp"
	// TODO multiple Ports
	log.Debug(pathLOG + "[createStartContainer] Setting ports ...")
	var p nat.Port
	if subapp.Containers[0].Ports == nil || len(subapp.Containers[0].Ports) == 0 {
		log.Warn(pathLOG + "[createStartContainer] No ports defined. Creating random ports...")
		rand.Seed(time.Now().UnixNano())
		min := 25000
		max := 30000
		randPort := rand.Intn(max - min + 1) + min
		p1, err := nat.NewPort("tcp", strconv.Itoa(randPort))
		if err != nil {
			return "", errors.New("ERROR (1) creating nat.Port object: " + err.Error())
		}
		p = p1

		log.Debug(pathLOG + "[createStartContainer] Using port " + strconv.Itoa(randPort) + " ...")

		// E2coAppContainerPorts
		ports := []structs.E2coAppContainerPorts{
			structs.E2coAppContainerPorts{
				Port: randPort,
				HostPort: randPort,
				ContainerPort: randPort,
				Protocol: "tcp",
			},
		}
		subapp.Containers[0].Ports = ports

	} else {
		p2, err := nat.NewPort(strings.ToLower(subapp.Containers[0].Ports[0].Protocol), strconv.Itoa(subapp.Containers[0].Ports[0].ContainerPort))
		if err != nil {
			return "", errors.New("ERROR (1) creating nat.Port object: " + err.Error())
		}
		p = p2
	}

	// ENVIRONMENT VARIABLES
	log.Debug(pathLOG + "[createStartContainer] Setting environment variables ...")
	var EnvVariables = make([]string, 0, len(subapp.Containers[0].Environment))

	log.Trace(pathLOG + "[createStartContainer] Checking docker specific environment variables ...")
	if len(subapp.Containers[0].EnvDocker) > 0 {
		for _, envvariables := range subapp.Containers[0].EnvDocker {
			log.Trace(pathLOG + "[createStartContainer] Name [" + envvariables.Name +"], Value [" + envvariables.Value +"]")
			EnvVariables = append(EnvVariables, envvariables.Name+"="+envvariables.Value)
		}
	} else {
		log.Debug(pathLOG + "[createStartContainer] No docker specific environment variables found")
		log.Trace(pathLOG + "[createStartContainer] Checking other environment variables defined in subapplication container ...")
	
		if len(subapp.Containers[0].Environment) > 0 {
			for _, envvariables := range subapp.Containers[0].Environment {
				log.Trace(pathLOG + "[createStartContainer] Name [" + envvariables.Name +"], Value [" + envvariables.Value +"]")
				EnvVariables = append(EnvVariables, envvariables.Name+"="+envvariables.Value)
			}
		} else {
			log.Debug(pathLOG + "[createStartContainer] No environment variables found")
		}
	}

	// EXPOSED PORTs and IPs
	log.Debug(pathLOG + "[createStartContainer] Exposing ports ...")
	expP, err := http.GetNewAvailablePort(orch)
	if err != nil {
		return "", errors.New("ERROR (1) creating nat.Port object: " + err.Error())
	}
	exposedPort := strconv.Itoa(expP)
	exposedIP := "0.0.0.0"

	log.Info(pathLOG + "[createStartContainer] Image: " + image + ", Port: " + p.Port())

	// 1. pull image
	log.Debug(pathLOG + "[createStartContainer] Image Pull ...")

	ctx := context.Background()

	// check if auth is needed
	if len(subapp.Containers[0].ImageRepoName) > 0 && len(subapp.Containers[0].ImageRepoPassword) > 0 {
		authConfig := types.AuthConfig{
			Username: subapp.Containers[0].ImageRepoName,
			Password: subapp.Containers[0].ImageRepoPassword,
		}
		encodedJSON, err := json.Marshal(authConfig)
		if err != nil {
			return "", errors.New("ERROR (2) creating authentication object - image repository: " + err.Error())
		}
		authStr := base64.URLEncoding.EncodeToString(encodedJSON)

		out, err := cli.ImagePull(ctx, image, types.ImagePullOptions{RegistryAuth: authStr})
		if err != nil {
			return "", errors.New("ERROR (3.1) pulling image [" + image + "]: " + err.Error())
		}
		io.Copy(os.Stdout, out)

	} else {
		out, err := cli.ImagePull(ctx, image, types.ImagePullOptions{})
		if err != nil {
			return "", errors.New("ERROR (3.2) pulling image [" + image + "]: " + err.Error())
		}
		io.Copy(os.Stdout, out)
	}

	// 2. create container
	// func (cli *Client) ContainerCreate(ctx context.Context, config *container.Config, hostConfig *container.HostConfig,
	//	networkingConfig *network.NetworkingConfig, platform *specs.Platform, containerName string)
	// ==> (container.ContainerCreateCreatedBody, error)
	log.Debug(pathLOG + "[createStartContainer] Creating container ...")
	resp, err := cli.ContainerCreate(
		ctx, // context.Context
		&container.Config{ // https://pkg.go.dev/github.com/docker/docker/api/types/container#Config
			Env: EnvVariables, 	// []string            // List of environment variable to set in the container
			//Cmd            	// strslice.StrSlice   // Command to run when starting the container
			//Volumes         	// map[string]struct{} // List of volumes (mounts) used for the container
			// 			Volumes: map[string]struct{}{mountPoint: {}}}
			//WorkingDir      	// string              // Current directory (PWD) in the command will be launched4
			//ExposedPorts    	//nat.PortSet         `json:",omitempty"` // List of exposed ports
			ExposedPorts: nat.PortSet{
				p: struct{}{},
			},
			Image: image,
		},
		&container.HostConfig{ // https://pkg.go.dev/github.com/docker/docker/api/types/container#HostConfig
			PortBindings: nat.PortMap{
				p: []nat.PortBinding{
					{
						HostIP:   exposedIP,
						HostPort: exposedPort,
					},
				},
			},
		},
		nil, // &network.NetworkingConfig{}  //	https://pkg.go.dev/github.com/docker/docker/api/types/network#NetworkingConfig
		nil, // ??? v1.Platform
		subapp.Name)
	if err != nil {
		return "", errors.New("ERROR (4) creating container: " + err.Error())
	}

	// 3. start container
	log.Debug(pathLOG + "[createStartContainer] Starting container ...")
	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return "", errors.New("ERROR (5) starting container: " + err.Error())
	}

	// Connect to the network
	// func (cli *Client) NetworkConnect(ctx context.Context, networkID, containerID string, config *network.EndpointSettings) error
	/*
		type EndpointSettings struct {
			// Configurations
			IPAMConfig *EndpointIPAMConfig
			Links      []string
			Aliases    []string
			// Operational data
			NetworkID           string
			EndpointID          string
			Gateway             string
			IPAddress           string
			IPPrefixLen         int
			IPv6Gateway         string
			GlobalIPv6Address   string
			GlobalIPv6PrefixLen int
			MacAddress          string
			DriverOpts          map[string]string
		}
	*/
	if len(subapp.Containers[0].NetworkName) > 0 {
		log.Debug(pathLOG + "[createStartContainer] Connecting container to defined network [" + subapp.Containers[0].NetworkName + "] ...")
		if err := cli.NetworkConnect(ctx, subapp.Containers[0].NetworkName, resp.ID, nil); err != nil {
			//return "", errors.New("ERROR (6) connecting to network: " + err.Error())
			log.Error(pathLOG + "[createStartContainer] ERROR (6) connecting to network: " + err.Error())
		}
	}
	
	log.Debug(pathLOG + "[createStartContainer] Container with ID [" + resp.ID + "] created and started.")
	return resp.ID, err
}
