//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 18 Mar 2021
// Updated on 06 Apr 2021
//
// @author: ATOS
//
package k8s

import (
	"errors"
	"encoding/json"
	"strconv"

	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/common/http"
	"atos.pledger/e2c-orchestrator/common/cfg"
	"atos.pledger/e2c-orchestrator/common"
	log "atos.pledger/e2c-orchestrator/common/logs"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Scaling-Engine > K8s "

/*
Adapter Adapter
*/
type Adapter struct{}

/*
ScaleOut Scale number of replicas
*/
func (a Adapter) ScaleOut(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	str, err :=  scaleInOut(task, orch, operation)
	if err != nil {
		log.Error(pathLOG+"[ScaleOut] ERROR: ", err)
	}
	return str, err
}

/*
ScaleIn Scale number of replicas
*/
func (a Adapter) ScaleIn(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	str, err :=  scaleInOut(task, orch, operation)
	if err != nil {
		log.Error(pathLOG+"[ScaleIn] ERROR: ", err)
	}
	return str, err
}

/*
ScaleUp Scale up application
*/
func (a Adapter) ScaleUp(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	str, err := scaleUpDown(task, orch, operation)
	if err != nil {
		log.Error(pathLOG+"[ScaleUp] ERROR: ", err)
	}
	return str, err
}

/*
ScaleDown Scale down applicatoin
*/
func (a Adapter) ScaleDown(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	str, err := scaleUpDown(task, orch, operation)
	if err != nil {
		log.Error(pathLOG+"[ScaleDown] ERROR: ", err)
	}
	return str, err
}

///////////////////////////////////////////////////////////////////////////////
// HORIZONTAL SCALABILITY: number of instances

// strToK8sScale Parses a string to a struct of type structs.K8sScale
func strToK8sScale(strData string) (structs.K8sScale, error) {
	var data structs.K8sScale
	err := json.Unmarshal([]byte(strData), &data)
	if err != nil {
		return data, errors.New("Error in json.Unmarshal function call: " + err.Error())
	}

	return data, nil
}

// getK8sScaleStruct: k8s: get scale info
func getK8sScaleStruct(task *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool) (structs.K8sScale, error) {
	log.Info(pathLOG + "[getK8sScaleStruct] Getting scaling info from task [" + task.ID + ", " + task.Name + "] ...")

	_, data, err := http.GetString(
		cfg.GetPathKubernetesScaleDeployment(orch, common.GetAppNamespace(*task, orch), task.ID),
		sec,
		orch.ConnectionToken)
	if err != nil {
		return structs.K8sScale{}, errors.New("Error in http GET call: " + err.Error())
	}

	log.Debug(pathLOG + "[getK8sScaleStruct] RESPONSE data: " + data)

	return strToK8sScale(data)
}

// updateK8sScale: k8s: update scale info => scale out / in
func updateK8sScale(task *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool, scaleObj structs.K8sScale) (string, error) {
	log.Info(pathLOG + "[updateK8sScale] Updating scaling info struct from subapplication [" + task.ID + ", " + task.Name + "] ...")
	
	status, _, err := http.PutStruct(
		cfg.GetPathKubernetesScaleDeployment(orch, common.GetAppNamespace(*task, orch), task.ID),
		sec,
		orch.ConnectionToken,
		scaleObj)
	if err != nil {
		return "", errors.New("Error in http PUT call: " + err.Error())
	}

	log.Debug(pathLOG + "[updateK8sScale] RESPONSE status: " + strconv.Itoa(status))

	return strconv.Itoa(status), nil
}

/*
scaleInOut Scale out / in the number of replicas of a task / application
*/
func scaleInOut(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Info(pathLOG + "[scaleInOut] Scaling in/out application [" + task.ID + ", " + task.Name + "] instances ...")

	// Auth Token needed?
	sec := common.AuthTokenNeeded(orch)

	// get scale object from Kubernetes deployment
	scaleObj, err := getK8sScaleStruct(task, orch, sec)
	if err != nil {
		return "", errors.New("Error getting Scale struct from K8s: " + err.Error())
	}

	// set new number of replicas
	scaleObj.Spec.Replicas = operation.Replicas

	// update / scale out/in K8s deployment
	status, err := updateK8sScale(task, orch, sec, scaleObj)
	if err == nil {
		// update task info 
		task.Replicas = scaleObj.Spec.Replicas
		return status, nil
	}
	
	log.Error(pathLOG+"[scaleInOut] ERROR scaling in/out application: ", err)

	err = errors.New("K8s Scale operation failed. status = [" + status + "]")
	return "", err
}

///////////////////////////////////////////////////////////////////////////////
// VERTICAL SCALABILITY: cpu, memory resources

// getK8sResourcesPatchStruct: k8s: get patch object
func getK8sResourcesPatchStruct(app *structs.E2coSubApp, operation structs.TaskOperation) (structs.PatchResourcesObj, error) {
	log.Info(pathLOG + "[getK8sResourcesPatchStruct] Getting patch object for subapplication [" + app.ID + ", " + app.Name + "] ...")

	if operation.Cpu == "" || operation.Memory == "" {
		return structs.PatchResourcesObj{}, errors.New("CPU / Memory not defined in Operation object")
	}

	var jsonPatch *structs.PatchResourcesObj
	jsonPatch = new(structs.PatchResourcesObj)
	jsonPatch.Spec.Template.Spec.Containers = make([]structs.PatchResourcesContainerObj, 1)
	
	jsonPatch.Spec.Template.Spec.Containers[0].Name = app.Containers[0].Name
	jsonPatch.Spec.Template.Spec.Containers[0].Image = app.Containers[0].Image
	jsonPatch.Spec.Template.Spec.Containers[0].Resources.Limits.Cpu = operation.Cpu
	jsonPatch.Spec.Template.Spec.Containers[0].Resources.Limits.Memory = operation.Memory
	jsonPatch.Spec.Template.Spec.Containers[0].Resources.Requests.Cpu = operation.Cpu
	jsonPatch.Spec.Template.Spec.Containers[0].Resources.Requests.Memory = operation.Memory
/*
	// containers
	totalContainers := len(app.Containers)
	log.Debug(pathLOG + "[getK8sResourcesPatchStruct] App containers: " + strconv.Itoa(totalContainers))
	//jsonPatch.Spec.Template.Spec.Containers = make([]structs.K8sDeploymentContainersResourcesPatch, totalContainers)
	for i := 0; i < totalContainers; i++ {
		elem := structs.PatchResourcesContainerObj{}
		elem.Name = app.ID
		elem.Resources.Limits.Cpu = operation.Cpu
		elem.Resources.Limits.Memory = operation.Memory
		elem.Resources.Requests.Cpu = operation.Cpu
		elem.Resources.Requests.Memory = operation.Memory

		jsonPatch.Spec.Template.Spec.Containers = append(jsonPatch.Spec.Template.Spec.Containers, elem)
		/*
		jsonPatch.Spec.Template.Spec.Containers[i].Name = app.ID
		jsonPatch.Spec.Template.Spec.Containers[i].Resources.Limits.Cpu = operation.Cpu
		jsonPatch.Spec.Template.Spec.Containers[i].Resources.Limits.Memory = operation.Memory
		jsonPatch.Spec.Template.Spec.Containers[i].Resources.Requests.Cpu = operation.Cpu
		jsonPatch.Spec.Template.Spec.Containers[i].Resources.Requests.Memory = operation.Memory
		*/
//	}

	str, err := common.StructToString(jsonPatch)
	if err == nil {
		log.Debug(pathLOG + "[getK8sResourcesPatchStruct] PatchResourcesObj object: " + str)
	} else {
		return structs.PatchResourcesObj{}, errors.New("Error creating PatchResourcesObj object")
	}

	return *jsonPatch, nil
}

// updateK8sVScale: k8s: update scale info => scale up / down
func updateK8sVScale(task *structs.E2coSubApp, orch structs.E2cOrchestrator, sec bool, patchObj structs.PatchResourcesObj) (string, error) {
	log.Info(pathLOG + "[updateK8sVScale] Updating scaling info struct from subapplication [" + task.ID + ", " + task.Name + "] ...")
	
	status, _, err := http.PatchMergeStruct(
		cfg.GetPathKubernetesVScaleDeployment(orch, common.GetAppNamespace(*task, orch), task.ID),
		sec,
		orch.ConnectionToken,
		patchObj)
	if err != nil {
		return "", errors.New("Error in http PATCH call: " + err.Error())
	}

	log.Debug(pathLOG + "[updateK8sVScale] RESPONSE status: " + strconv.Itoa(status))

	return strconv.Itoa(status), nil
}

/*
scaleUpDown Scale up / down the resources (i.e. cpu, memory) of a sub application
*/
func scaleUpDown(app *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Info(pathLOG + "[scaleUpDown] Scaling up/down application [" + app.ID + ", " + app.Name + "] resources ...")

	// Auth Token needed?
	sec := common.AuthTokenNeeded(orch)

	// get scale patch object
	patchObj, err := getK8sResourcesPatchStruct(app, operation)
	if err != nil {
		return "", errors.New("Error getting patch object: " + err.Error())
	}

	// update / scale out/in K8s deployment
	// ==> PATCH /apis/apps/v1/namespaces/{namespace}/deployments/{name}
	status, err := updateK8sVScale(app, orch, sec, patchObj)
	if err == nil {
		// update app info 
		// containers
		totalContainers := len(app.Containers)
		for i := 0; i < totalContainers; i++ {
			app.Containers[i].Hw.Memory = operation.Memory
			app.Containers[i].Hw.Cpu = operation.Cpu
		}
		return status, nil
	}
	
	log.Error(pathLOG+"[scaleUpDown] ERROR scaling up/down application: ", err)

	err = errors.New("K8s Vertical Scale operation failed. status = [" + "status" + "]")
	return "", err
}
