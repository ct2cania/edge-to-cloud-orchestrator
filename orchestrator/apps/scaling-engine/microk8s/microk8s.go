//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 18 Mar 2021
// Updated on 18 Mar 2021
//
// @author: ATOS
//
package microk8s

import (
	"atos.pledger/e2c-orchestrator/data/structs"
	k8sDeplEng "atos.pledger/e2c-orchestrator/orchestrator/apps/scaling-engine/k8s"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Scaling-Engine > MicroK8s "

/*
Adapter Adapter
*/
type Adapter struct{}

/*
ScaleOut Scale number of replicas
*/
func (a Adapter) ScaleOut(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	adapter := k8sDeplEng.Adapter{}
	return adapter.ScaleOut(task, orch, operation)
}

/*
ScaleIn Scale number of replicas
*/
func (a Adapter) ScaleIn(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	adapter := k8sDeplEng.Adapter{}
	return adapter.ScaleIn(task, orch, operation)
}

/*
ScaleUp Scale up application
*/
func (a Adapter) ScaleUp(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	adapter := k8sDeplEng.Adapter{}
	return adapter.ScaleUp(task, orch, operation)
}

/*
ScaleDown Scale down applicatoin
*/
func (a Adapter) ScaleDown(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	adapter := k8sDeplEng.Adapter{}
	return adapter.ScaleDown(task, orch, operation)
}
