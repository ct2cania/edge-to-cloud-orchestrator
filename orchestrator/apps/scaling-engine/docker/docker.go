//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 30 Mar 2021
// Updated on 30 Mar 2021
//
// @author: ATOS
//
package docker

import (
	"atos.pledger/e2c-orchestrator/data/structs"
	log "atos.pledger/e2c-orchestrator/common/logs"
)

// path used in logs
const pathLOG string = "E2CO > Orchestrator > Scaling-Engine > Docker "

/*
Adapter Adapter
*/
type Adapter struct{}

/*
ScaleOut Scale number of replicas
*/
func (a Adapter) ScaleOut(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Warn(pathLOG + "[ScaleOut] Function Not Implemented")
	return "", nil
}

/*
ScaleIn Scale number of replicas
*/
func (a Adapter) ScaleIn(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Warn(pathLOG + "[ScaleIn] Function Not Implemented")
	return "", nil
}

/*
ScaleUp Scale up application
*/
func (a Adapter) ScaleUp(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Warn(pathLOG + "[ScaleUp] Function Not Implemented")
	return "", nil
}

/*
ScaleDown Scale down applicatoin
*/
func (a Adapter) ScaleDown(task *structs.E2coSubApp, orch structs.E2cOrchestrator, operation structs.TaskOperation) (string, error) {
	log.Warn(pathLOG + "[ScaleDown] Function Not Implemented")
	return "", nil
}