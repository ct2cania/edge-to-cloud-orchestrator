//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 16 Sep 2021
// Updated on 16 Sep 2021
//
// @author: ATOS
//
package kafkadss

import (
	"errors"
	"strconv"
	"strings"
	"time"

	"atos.pledger/e2c-orchestrator/common/globals"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/data/manifests"
	"atos.pledger/e2c-orchestrator/common"
)

/*
parseDeploymentManifestToE2CO Mapping K8S Deployment manifest to E2CO JSON model
*/
func parseDeploymentManifestToE2CO(k8sDeployment manifests.K8SDeployment, e2coApp *structs.E2coApplication) {
	log.Trace(pathLOG + "[parseDeploymentManifestToE2CO] 'Mapping K8S Deployment manifest to E2CO JSON model ...")

	// PLACEHOLDERs
	// Namespace ==> PLACEHOLDER?
	if k8sDeployment.Metadata.Namespace != "" {
		log.Trace(pathLOG + "[parseDeploymentManifestToE2CO] >>> Setting Namespace to '" + k8sDeployment.Metadata.Namespace + "'")
		// k8sDeployment.Metadata.Namespace is a PLACEHOLDER
		//if strings.HasPrefix(strings.ToLower(k8sDeployment.Metadata.Namespace), strings.ToLower(globals.PLACEHOLDER_PREFIX)) {
		//	e2coApp.Locations[0].NameSpace = globals.PLACEHOLDER_STRING_VALUE
		//} else {
		e2coApp.Locations[0].NameSpace = k8sDeployment.Metadata.Namespace
		//}
	}

	// sub apps locations
	e2coApp.Apps[0].Locations = e2coApp.Locations
	e2coApp.Apps[0].LocationsType = globals.V_GLOBAL

	// Replicas ==> PLACEHOLDER?
	log.Trace(pathLOG + "[parseDeploymentManifestToE2CO] Checking if 'k8sDeployment.Spec.Replicas' is a number ...")
	if n, err := strconv.Atoi(k8sDeployment.Spec.Replicas); err == nil {
		// k8sDeployment.Spec.Replicas is a Number
		log.Trace(pathLOG + "[parseDeploymentManifestToE2CO] 'k8sDeployment.Spec.Replicas' is a number")
		if n >= 0 {
			e2coApp.Replicas = n
		} else {
			e2coApp.Replicas = 1
		}
	} else {
		// if not a number, we assume k8sDeployment.Spec.Replicas is a PLACEHOLDER
		log.Trace(pathLOG + "[parseDeploymentManifestToE2CO] 'k8sDeployment.Spec.Replicas' is NOT a number. Setting PLACEHOLDER ...")
		e2coApp.Replicas = globals.PLACEHOLDER_INT_VALUE
	}

	// NodeSelector ==> PLACEHOLDER?
	// TODO
	// NodeSelector
	e2coApp.Apps[0].NodeSelector = map[string]string{}
	for key, value := range k8sDeployment.Spec.Template.Spec.NodeSelector {
		e2coApp.Apps[0].NodeSelector[key] = value
	}

	// sub apps replicas
	e2coApp.Apps[0].ReplicasType = globals.V_GLOBAL
	e2coApp.Apps[0].Replicas = e2coApp.Replicas

	if k8sDeployment.Spec.Template.Spec.Containers == nil {
		log.Warn(pathLOG + "[parseDeploymentManifestToE2CO] No Spec.Containers content found.")
	}

	// Rest of the fields
	// Containers
	for key := range k8sDeployment.Spec.Template.Spec.Containers {
		var e2coAppContainer structs.E2coAppContainer
		if len(k8sDeployment.Spec.Template.Spec.Containers[key].ImagePullSecret) > 0 {
			e2coAppContainer.ImagePullSecret = k8sDeployment.Spec.Template.Spec.Containers[key].ImagePullSecret[0] //?
		}
		e2coAppContainer.ImagePullSecrets = k8sDeployment.Spec.Template.Spec.Containers[key].ImagePullSecrets
		e2coAppContainer.ImageRepoName = k8sDeployment.Spec.Template.Spec.Containers[key].ImageRepoName
		e2coAppContainer.ImageRepoPassword = k8sDeployment.Spec.Template.Spec.Containers[key].ImageRepoPassword
		e2coAppContainer.NetworkName = k8sDeployment.Spec.Template.Spec.Containers[key].NetworkName

		// ImagePullPolicy
		e2coAppContainer.ImagePullPolicy = k8sDeployment.Spec.Template.Spec.Containers[key].ImagePullPolicy

		//e2coTaskExtendedContainer.Name = k8sDeployment.Metadata.Name
		e2coAppContainer.Name = k8sDeployment.Spec.Template.Spec.Containers[key].Name
		e2coAppContainer.Image = k8sDeployment.Spec.Template.Spec.Containers[key].Image

		// Environment variables
		for key2 := range k8sDeployment.Spec.Template.Spec.Containers[key].Env {
			var e2coAppContainerEnv structs.E2coEnvironmentVars
			e2coAppContainerEnv.Name = k8sDeployment.Spec.Template.Spec.Containers[key].Env[key2].Name
			e2coAppContainerEnv.Value = k8sDeployment.Spec.Template.Spec.Containers[key].Env[key2].Value
			e2coAppContainer.Environment = append(e2coAppContainer.Environment, e2coAppContainerEnv)
		}

		// Environment variables - DOCKER apps
		for key2 := range k8sDeployment.Spec.Template.Spec.Containers[key].EnvDocker {
			var e2coAppContainerEnvDocker structs.E2coEnvironmentVars
			e2coAppContainerEnvDocker.Name = k8sDeployment.Spec.Template.Spec.Containers[key].EnvDocker[key2].Name
			e2coAppContainerEnvDocker.Value = k8sDeployment.Spec.Template.Spec.Containers[key].EnvDocker[key2].Value
			e2coAppContainer.EnvDocker = append(e2coAppContainer.EnvDocker, e2coAppContainerEnvDocker)
		}

		// Labels in spec.template.metadata
		for key2, value2 := range k8sDeployment.Spec.Template.Metadata.Labels {
			var label structs.E2coAppLabel
			label.Key = key2
			label.Value = value2
			e2coAppContainer.Labels = append(e2coAppContainer.Labels, label)
		}

		// Ports
		for key2 := range k8sDeployment.Spec.Template.Spec.Containers[key].Ports {
			var e2coAppContainerPorts structs.E2coAppContainerPorts
			e2coAppContainerPorts.ContainerPort = k8sDeployment.Spec.Template.Spec.Containers[key].Ports[key2].ContainerPort
			e2coAppContainerPorts.HostPort = k8sDeployment.Spec.Template.Spec.Containers[key].Ports[key2].HostPort
			e2coAppContainerPorts.Protocol = k8sDeployment.Spec.Template.Spec.Containers[key].Ports[key2].Protocol
			e2coAppContainer.Ports = append(e2coAppContainer.Ports, e2coAppContainerPorts)
		}

		// Commands
		e2coAppContainer.Command = append(e2coAppContainer.Command, k8sDeployment.Spec.Template.Spec.Containers[key].Command...)

		// Args
		e2coAppContainer.Args = append(e2coAppContainer.Args, k8sDeployment.Spec.Template.Spec.Containers[key].Args...)

		// Resources?
		// CPU ==> PLACEHOLDER?
		// e2coAppContainer.Hw.Cpu = k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Cpu
		if k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Cpu != "" {
			log.Trace(pathLOG + "[parseDeploymentManifestToE2CO] >>> Setting Cpu to '" + k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Cpu + "'")
			// k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Cpu is a PLACEHOLDER
			//if strings.HasPrefix(strings.ToLower(k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Cpu), strings.ToLower(globals.PLACEHOLDER_PREFIX)) {
			//	e2coAppContainer.Hw.Cpu = globals.PLACEHOLDER_STRING_VALUE
			//} else {
			e2coAppContainer.Hw.Cpu = k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Cpu
			//}
		}

		// CPU ==> PLACEHOLDER?
		// e2coAppContainer.Hw.Memory = k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Memory
		if k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Memory != "" {
			log.Trace(pathLOG + "[parseDeploymentManifestToE2CO] >>> Setting Memory to '" + k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Memory + "'")
			// k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Cpu is a PLACEHOLDER
			//if strings.HasPrefix(strings.ToLower(k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Memory), strings.ToLower(globals.PLACEHOLDER_PREFIX)) {
			//	e2coAppContainer.Hw.Memory = globals.PLACEHOLDER_STRING_VALUE
			//} else {
			e2coAppContainer.Hw.Memory = k8sDeployment.Spec.Template.Spec.Containers[key].Resources.Requests.Memory
			//}
		}

		// VolumeMounts
		for _, value2 := range k8sDeployment.Spec.Template.Spec.Containers[key].VolumeMounts {
			var e2coAppContainerVolumeMounts structs.E2coAppContainerVolumes
			e2coAppContainerVolumeMounts.Name = value2.Name
			e2coAppContainerVolumeMounts.MountPath = value2.MountPath
			e2coAppContainerVolumeMounts.ReadOnly = value2.ReadOnly
			e2coAppContainer.Volumes = append(e2coAppContainer.Volumes, e2coAppContainerVolumeMounts)
		}
		//
		e2coApp.Apps[0].Containers = append(e2coApp.Apps[0].Containers, e2coAppContainer)
	} // for containers

	// Labels in Deployment metadata
	for key, value := range k8sDeployment.Metadata.Labels {
		var label structs.E2coAppLabel
		label.Key = key
		label.Value = value
		e2coApp.Apps[0].Labels = append(e2coApp.Apps[0].Labels, label)
	}

	// Volumes?
	for _, value := range k8sDeployment.Spec.Template.Spec.Volumes {
		var e2coAppVolume structs.E2coAppContainerVolumes
		e2coAppVolume.Name = value.Name
		e2coApp.Apps[0].Volumes = append(e2coApp.Apps[0].Volumes, e2coAppVolume)
	}
}

/*
parseDockerDescriptorToE2CO Mapping K8S Deployment manifest to E2CO JSON model
*/
func parseDockerDescriptorToE2CO(dockerComposeDeployment manifests.DockerComposeDeployment, e2coApp *structs.E2coApplication) {
	log.Trace(pathLOG + "[parseDockerDescriptorToE2CO] 'Mapping Docker Deployment manifest to E2CO JSON model ...")

	// Descriptor example: "deployDescriptor": "version: \"3\" services: basic-analytics-rabbitmq: image: rabbitmq:3 ports: - \"5672\""
	// Services
	for key, value := range dockerComposeDeployment.Services {
		var e2coAppContainer structs.E2coAppContainer
		e2coAppContainer.Name = key
		e2coAppContainer.Image = value.Image

		// Environment variables
		for _, value2 := range value.Environment {
			var e2coAppContainerEnv structs.E2coEnvironmentVars
			if len(strings.Split(value2, "=")) == 2 { // services.<service_name>.environment: [node=production]
				e2coAppContainerEnv.Name = strings.Split(value2, "=")[0]
				e2coAppContainerEnv.Value = strings.Split(value2, "=")[1]
				e2coAppContainer.Environment = append(e2coAppContainer.Environment, e2coAppContainerEnv)
			}
		}

		// Labels
		for _, value2 := range value.Labels {
			var label structs.E2coAppLabel
			if len(strings.Split(value2, "=")) == 2 { // services.<service_name>.deploy.labels: [app=recommender]
				label.Key = strings.Split(value2, "=")[0]
				label.Value = strings.Split(value2, "=")[1]
				e2coAppContainer.Labels = append(e2coAppContainer.Labels, label)
			}
		}

		// Ports
		for _, value2 := range value.Ports {
			var e2coAppContainerPorts structs.E2coAppContainerPorts
			n, err2 := strconv.Atoi(value2)
			if err2 == nil && n > 0 {
				e2coAppContainerPorts.ContainerPort = n
				e2coAppContainer.Ports = append(e2coAppContainer.Ports, e2coAppContainerPorts)
			}
		}
		
		// Replicas
		if value.Deploy.Replicas > 0 {
			e2coApp.Replicas = value.Deploy.Replicas
		}
		e2coApp.Apps[0].Containers = append(e2coApp.Apps[0].Containers, e2coAppContainer)
	}
}

///////////////////////////////////////////////////////////////////////////////

func checkMapValues(m map[string]interface{}) error {
	log.Debug(pathLOG + "[checkMapValues] Checking app info values ...")

	if m["name"] == nil || m["app"] == nil || m["deployDescriptor"] == nil {
		return errors.New("Map is missing one or more fields content: name, app, deployDescriptor")
	}

	return nil
}

/*
ToE2coApplicationV2 Creates an E2coApplication struct from data comming from DSS - Config Service
	1. main fields are taken from map
	2. sub apps
	3. rest of the fields are taken from deployDescriptor field ==> PLACEHOLDERS
	4. 
*/
func ToE2coApplication(id string, m map[string]interface{}) (structs.E2coApplication, error) {
	log.Debug(pathLOG + "[ToE2coApplicationV2] Creating a new E2coApplication from app info values ...")

	var err error
	var e2coApp structs.E2coApplication

	err = checkMapValues(m)
	if err != nil {
		log.Error(pathLOG + "[ToE2coApplicationV2] Error checking map values: ", err)
		return structs.E2coApplication{}, err
	}

	///////////////////////////////////
	// 1. Main fields
	log.Debug(pathLOG + "[ToE2coApplicationV2] Creating main fields ...")

	// ID
	e2coApp.ID = id
	e2coApp.Name = m["name"].(string)

	// E2coApplication.Locations
	e2coApp.Locations = make([]structs.E2coLocation, 1) // locations list

	e2coApp.Locations[0].NameSpace = globals.NOT_DEFINED  // ==> deployDescriptor
	e2coApp.Locations[0].Description = globals.NOT_DEFINED
	e2coApp.Locations[0].Nodes = []string{globals.NOT_DEFINED}  // ==> deployDescriptor

	// E2coApplication.Locations.IDE2cOrchestrator
	e2coApp.Locations[0].IDE2cOrchestrator = globals.NOT_DEFINED
	if m["initialConfiguration"] == nil {
		log.Warn(pathLOG + "[ToE2coApplicationV2] Error reading app info: configuration is empty")
	} else {
		var configuration map[string]interface{}
		aux := m["initialConfiguration"].(string)

		if err = common.ParseJSONObj(aux, &configuration); err != nil || configuration == nil {
			return structs.E2coApplication{}, errors.New("Error unmarshalling JSON configuration info")
		}

		if configuration["infrastructure_id"] != nil {
			e2coApp.Locations[0].IDE2cOrchestrator = configuration["infrastructure_id"].(string)
		} else {
			log.Warn(pathLOG + "[ToE2coApplicationV2] IDE2cOrchestrator not defined.")
		}
	}

	// Replicas
	e2coApp.Replicas = globals.NOT_DEFINED_INT // ==> deployDescriptor

	// Created & Updated
	t := time.Now().String()
	e2coApp.Created = t
	e2coApp.Updated = t

	///////////////////////////////////
	// 2. sub apps
	log.Debug(pathLOG + "[ToE2coApplicationV2] Creating sub applications ...")

	// E2coApplication.E2coSubApp
	e2coApp.Apps = make([]structs.E2coSubApp, 1) // apps list

	mApp := m["app"].(map[string]interface{})

	e2coApp.Apps[0].ID = common.GetStringValueFromNumber(mApp["id"]) 
	e2coApp.Apps[0].Name = mApp["name"].(string)
	e2coApp.Apps[0].ParentID = id

	e2coApp.Apps[0].Type = m["deployType"].(string)
	if strings.ToLower(e2coApp.Apps[0].Type) == "kubernetes" || strings.ToLower(e2coApp.Apps[0].Type) == "k8s" {
		e2coApp.Apps[0].Type = globals.KUBERNETES //"k8s"
	} else if strings.ToLower(e2coApp.Apps[0].Type) == "docker" {
		e2coApp.Apps[0].Type = globals.DOCKER //"docker"
	} else {
		log.Warn(pathLOG + "[ToE2coApplicationV2] App type not defined / expected: " + e2coApp.Apps[0].Type + ". Using default value: " + structs.DEFAULT_APP_TYPE)
		e2coApp.Apps[0].Type = structs.DEFAULT_APP_TYPE
	}

	// parent ID = Main ID
	e2coApp.Apps[0].ParentID = e2coApp.ID

	// Created & Updated
	e2coApp.Apps[0].Created = t
	e2coApp.Apps[0].Updated = t

	// locations
	e2coApp.Apps[0].Locations = e2coApp.Locations
	e2coApp.Apps[0].LocationsType = globals.V_GLOBAL

	// replicas
	e2coApp.Apps[0].ReplicasType = globals.V_GLOBAL
	e2coApp.Apps[0].Replicas = e2coApp.Replicas

	// environment variables
	// Qos
	// exposed?

	// YamlJSON
	e2coApp.Apps[0].YamlJSON = m["deployDescriptor"].(string)

	// MaxReplicas
	e2coApp.Apps[0].MaxReplicas = structs.DEFAULT_APP_MAX_REPLICAS

	///////////////////////////////////
	// 3. deployDescriptor ==> PLACEHOLDERS
	if m["deployDescriptor"] == nil {
		log.Error(pathLOG + "[ToE2coApplicationV2] deployDescriptor not found!")
		return structs.E2coApplication{}, errors.New("deployDescriptor not found") 
	}

	if e2coApp.Apps[0].Type == globals.KUBERNETES || e2coApp.Apps[0].Type == globals.DOCKER { // "k8s"
		//	Kubernetes descriptor
		var k8sDeployment manifests.K8SDeployment
		var aux string = m["deployDescriptor"].(string)

		if err := common.ParseYAMLObj(aux, &k8sDeployment); err != nil {
			return structs.E2coApplication{}, err
		}

		parseDeploymentManifestToE2CO(k8sDeployment, &e2coApp)

	} /*else if e2coApp.Apps[0].Type == globals.DOCKER { //"docker"
		// 	Docker descriptor
		var dockerComposeDeployment manifests.DockerComposeDeployment
		var aux string = m["deployDescriptor"].(string)

		if err := common.ParseYAMLObj(aux, &dockerComposeDeployment); err != nil {
			return structs.E2coApplication{}, err
		}

		parseDockerDescriptorToE2CO(dockerComposeDeployment, &e2coApp)
	}*/

	return e2coApp, nil
}

/*
ToE2coSoeObject Creates a "SOE" E2coApplication struct from data comming from DSS - Config Service
*/
func ToE2coSoeObject(soeObj structs.E2coSoeObject) (structs.E2coApplication, error) {
	log.Debug(pathLOG + "[ToE2coSoeObject] Creating a new E2coApplication from app info values ...")

	var e2coApp structs.E2coApplication

	///////////////////////////////////
	// 1. Main fields
	log.Debug(pathLOG + "[ToE2coSoeObject] Creating main fields ...")

	// ID
	e2coApp.ID = soeObj.IDE2cOrchestrator + "_SOE"
	e2coApp.Name = soeObj.Name

	// SoeObj
	//e2coApp.SoeObj = soeObj

	// E2coApplication.Locations
	e2coApp.Locations = make([]structs.E2coLocation, 1) // locations list

	e2coApp.Locations[0].NameSpace = globals.NOT_DEFINED  // ==> deployDescriptor
	e2coApp.Locations[0].Description = globals.NOT_DEFINED
	e2coApp.Locations[0].Nodes = []string{globals.NOT_DEFINED}  // ==> deployDescriptor

	// E2coApplication.Locations.IDE2cOrchestrator
	if len(soeObj.IDE2cOrchestrator) > 0 {
		e2coApp.Locations[0].IDE2cOrchestrator = soeObj.IDE2cOrchestrator
	} else {
		e2coApp.Locations[0].IDE2cOrchestrator = globals.NOT_DEFINED
	}
	
	// Replicas
	e2coApp.Replicas = globals.NOT_DEFINED_INT // ==> deployDescriptor

	// Created & Updated
	t := time.Now().String()
	e2coApp.Created = t
	e2coApp.Updated = t

	return e2coApp, nil
}