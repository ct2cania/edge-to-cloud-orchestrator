//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 26 Mar 2021
//
// @author: ATOS
//
package memory

import (
	"strings"
	"errors"
    "time"

	"atos.pledger/e2c-orchestrator/common"
	log "atos.pledger/e2c-orchestrator/common/logs"
	"atos.pledger/e2c-orchestrator/data/structs"
	"atos.pledger/e2c-orchestrator/rest-api/sec/models"
	cfg "atos.pledger/e2c-orchestrator/common/cfg"

	"github.com/tidwall/buntdb"
)

// path used in logs
const pathLOG string = "E2CO > Data > DB > Memory "

/*
Adapter Adapter
*/
type Adapter struct{}

// PREFIX used in database
const dbE2coTaskPREFIX = "APPL"
const dbE2cOrchestratorPREFIX = "ORCH"
const dbE2coAppPREFIX = "SAPP"
const dbE2coUserPREFIX = "USER"
const dbLogsPREFIX = "LOGS"
const dbSoePREFIX = "SOE"

/*
E2CODB DATABASE
*/
var E2CODB *buntdb.DB

/*
Initialize initialization
*/
func (a Adapter) Initialize() error {
	log.Info(pathLOG + "[Initialize] Initializating connection to default Database ...")

	err := errors.New("")
	E2CODB, err = InitDB()
	
	return err
}

/*
InitDB  initializes database
*/
func InitDB() (*buntdb.DB, error) {
	log.Info(pathLOG + "[InitDB] Initializating Database ...")

	repository := ":memory:" // By default, open a file that does not persist to disk.
	if len(cfg.Config.DatabaseRepository) > 0 {
		repository = cfg.Config.DatabaseRepository
	}

	db, err := buntdb.Open(repository)
	if err != nil {
		log.Error(pathLOG+"[InitDB] Error openning the database", err)
		return nil, err
	}
	return db, nil
}

/*
setValue insert / update operation
*/
func setValue(id string, ct interface{}, prefix string) error {
	id = strings.Replace(id, prefix, "", 1)

	log.Trace(pathLOG + "[setValue] Setting value [" + id + "] [" + prefix + "] ...")

	content, err := common.StructToString(ct)
	if err == nil {
		err = E2CODB.Update(func(tx *buntdb.Tx) error {
			if err != nil {
				log.Error(pathLOG+"[setValue] ERROR ", err)
			}
			_, _, err := tx.Set(prefix+id, content, nil)
			return err
		})
	}

	return err
}

/*
deleteValue delete operation
*/
func deleteValue(id string, prefix string) (string, error) {
	id = strings.Replace(id, prefix, "", 1)

	err := E2CODB.Update(func(tx *buntdb.Tx) error {
		res, err := tx.Delete(prefix + id)
		if err != nil {
			log.Warn(pathLOG+"[deleteValue] ERROR ", err)
			return err
		}

		log.Debug(pathLOG + "[deleteValue] Application (" + prefix + ")" + id + " deleted [" + res + "]")
		return err
	})

	return id, err
}

/*
CloseDB Closes database
*/
func (a Adapter) CloseConnection() {
	log.Info(pathLOG + "[CloseDB] Closing Database ...")
	defer E2CODB.Close()
}

///////////////////////////////////////////////////////////////////////////////
// E2CO Users - Security

/*
Signup creates a user in db
*/
func (a Adapter) Signup(u models.User) error {
	user, _ := a.GetUser(u.Email)
	if user == (models.User{}) {
		return setValue(u.Email, u, dbE2coUserPREFIX)
	}

	log.Error(pathLOG + "[Signup] user: ", user)
	return errors.New("User (email) already exists")
}

/*
Login logs users in
*/
func (a Adapter) Login(u models.LoginPayload) (models.User, error) {
	dbUser := models.User{}
	err := E2CODB.View(func(tx *buntdb.Tx) error {
		dbUserStr, err := tx.Get(dbE2coUserPREFIX + u.Email)
		if err != nil {
			log.Warn(pathLOG+"[Login] Warning: User not found in DB: ", err)
			return err
		}

		log.Trace(pathLOG + "[Login] [User=" + dbUserStr + "]")
		dbUser, err = structs.StringToE2coUser(dbUserStr)
		return err
	})

	return dbUser, err
}

/*
GetUsers get the list of users
*/
func (a Adapter) GetUsers() ([]models.User, error) {
	log.Trace(pathLOG + "[GetUsers] Getting users ...")
	var dbUsers []models.User

	err := E2CODB.View(func(tx *buntdb.Tx) error {
		err2 := tx.Ascend("", func(key, value string) bool {
			log.Trace(pathLOG + ">>>> key: " + key + ", value: " + value)

			dbRes, err := structs.StringToE2coUser(value)
			if err == nil && strings.HasPrefix(key, dbE2coUserPREFIX) {
				dbRes.Password = "******"
				dbUsers = append(dbUsers, dbRes)
			}

			return true
		})
		return err2
	})

	return dbUsers, err
}

/*
GetUsers get a user
*/
func (a Adapter) GetUser(id string) (models.User, error) {
	dbUser := models.User{}
	err := E2CODB.View(func(tx *buntdb.Tx) error {
		dbRes, err := tx.Get(dbE2coUserPREFIX + id)
		if err != nil {
			log.Warn(pathLOG+"[GetUser] Warning: User not found in DB: ", err)
			return err
		}

		log.Trace(pathLOG + "[GetUser] [User=" + dbRes + "]")
		dbUser, err = structs.StringToE2coUser(dbRes)
		return err
	})

	if err == nil {
		dbUser.Password = "******"
	}
	
	return dbUser, err
}


///////////////////////////////////////////////////////////////////////////////
// Applications

// E2coApplication

/*
SetE2coApplication adds a anew application / updates application
*/
func (a Adapter) SetE2coApplication(id string, dbtask structs.E2coApplication) error {
	return setValue(id, dbtask, dbE2coTaskPREFIX)
}

/*
UpdateE2coApplication adds a anew application / updates application
*/
func (a Adapter) UpdateE2coApplication(id string, dbtask structs.E2coApplication) error {
	return setValue(id, dbtask, dbE2coTaskPREFIX)
}

/*
ReadE2coApplication gets an application from database
*/
func (a Adapter) ReadE2coApplication(id string) (structs.E2coApplication, error) {
	id = strings.Replace(id, dbE2coTaskPREFIX, "", 1)

	dbtask := structs.E2coApplication{}
	err := E2CODB.View(func(tx *buntdb.Tx) error {
		dbtaskStr, err := tx.Get(dbE2coTaskPREFIX + id)
		if err != nil {
			log.Warn(pathLOG+"[ReadE2coApplication] Warning: Application not found in DB: ", err)
			return err
		}

		log.Trace(pathLOG + "[ReadE2coApplication] [E2coApp=" + dbtaskStr + "]")
		dbtask, err = structs.StringToE2coApplication(dbtaskStr)
		return err
	})

	return dbtask, err
}

/*
DeleteE2coApplication removes an application from database
*/
func (a Adapter) DeleteE2coApplication(id string) (string, error) {
	// get subapps
	app, err := a.ReadE2coApplication(id)
	if err == nil && len(app.Apps) > 0 {
		for _, subapp := range app.Apps {
			a.DeleteE2coSubApp(subapp.ID)
		}
	}
	
	// delete app
	return deleteValue(id, dbE2coTaskPREFIX)
}

/*
ReadAllApplications Return all applications from database
*/
func (a Adapter) ReadAllApplications() ([]structs.E2coApplication, error) {
	log.Trace(pathLOG + "[ReadAllApplications] Getting All apps ...")
	var dbtasks []structs.E2coApplication

	err := E2CODB.View(func(tx *buntdb.Tx) error {
		err2 := tx.Ascend("", func(key, value string) bool {
			log.Trace(pathLOG + ">>>> key: " + key + ", value: " + value)

			dbtask, err := structs.StringToE2coApplication(value)
			if err == nil && strings.HasPrefix(key, dbE2coTaskPREFIX) {
				dbtasks = append(dbtasks, dbtask)
			}

			return true
		})
		return err2
	})

	return dbtasks, err
}

// updateSubAppFromApp
func updateSubAppFromApp(app *structs.E2coApplication, subapp structs.E2coSubApp) {
	log.Trace(pathLOG+"[updateSubAppFromApp] Updating sub application [" + subapp.ID + "] in app [" + app.ID + "] ...")
	for i, s := range app.Apps {
		if s.ID == subapp.ID {
			app.Apps[i] = subapp
			log.Debug(pathLOG+"[updateSubAppFromApp] Application updated: sub application [" + s.ID + "] updated")
			return
		}
	}

	log.Warn(pathLOG+"[updateSubAppFromApp] Warning: Application was not updated: sub application [" + subapp.ID + "] not found")
}

///////////////////////////////////////////////////////////////////////////////
// Sub Applications

// E2coApp

/*
SetE2coApp adds a anew application / updates application
*/
func (a Adapter) SetE2coSubApp(id string, subapp structs.E2coSubApp) error {
	log.Trace(pathLOG+"[SetE2coSubApp] Updating sub application [" + subapp.ID + "] ...")
	err := setValue(id, subapp, dbE2coAppPREFIX)
	if err == nil {
		// update app
		app, err := a.ReadE2coApplication(subapp.ParentID)
		if err == nil {
			updateSubAppFromApp(&app, subapp)
			return a.SetE2coApplication(app.ID, app)
		}
	}

	return err
}

/*
UpdateE2coSubApp adds a anew application / updates application
*/
func (a Adapter) UpdateE2coSubApp(id string, subapp structs.E2coSubApp) error {
	return a.SetE2coSubApp(id, subapp)
}

/*
ReadE2coApp gets an application from database
*/
func (a Adapter) ReadE2coSubApp(id string) (structs.E2coSubApp, error) {
	id = strings.Replace(id, dbE2coAppPREFIX, "", 1)

	dbtask := structs.E2coSubApp{}
	err := E2CODB.View(func(tx *buntdb.Tx) error {
		dbtaskStr, err := tx.Get(dbE2coAppPREFIX + id)
		if err != nil {
			log.Warn(pathLOG+"[ReadE2coSubApp] Warning: Application [" + id + "] not found in DB: ", err)
			return err
		}

		log.Trace(pathLOG + "[ReadE2coSubApp] [E2coApp=" + dbtaskStr + "]")
		dbtask, err = structs.StringToE2coSubApp(dbtaskStr)
		return err
	})

	return dbtask, err
}

/*
DeleteE2coApp removes an application from database
*/
func (a Adapter) DeleteE2coSubApp(id string) (string, error) {
	//  get parent
	subapp, err := a.ReadE2coSubApp(id)
	if err == nil {
		// delete subapp from parent struct
		app, err := a.ReadE2coApplication(subapp.ParentID)
		if err == nil {
			index := -1
			for i, s := range app.Apps {
				if s.ID == subapp.ID {
					index = i
					break;
				}
			}

			if index > -1 {
				// remove from array
				app.Apps = removeIndex(app.Apps, index)
				// update app
				err = a.SetE2coApplication(app.ID, app)
			}
		}
	}

	// delete subapp
	return deleteValue(id, dbE2coAppPREFIX)
}

// removeIndex remove subapp from array
func removeIndex(s []structs.E2coSubApp, index int) []structs.E2coSubApp {
	return append(s[:index], s[index+1:]...)
}

/*
ReadAllSubApplications Return all sub applications from database
*/
func (a Adapter) ReadAllSubApplications() ([]structs.E2coSubApp, error) {
	log.Trace(pathLOG + "[ReadAllSubApplications] Getting All sub apps ...")
	var dbtasks []structs.E2coSubApp

	err := E2CODB.View(func(tx *buntdb.Tx) error {
		err2 := tx.Ascend("", func(key, value string) bool {
			log.Trace(pathLOG + ">>>> key: " + key + ", value: " + value)

			dbtask, err := structs.StringToE2coSubApp(value)
			if err == nil && strings.HasPrefix(key, dbE2coAppPREFIX) {
				dbtasks = append(dbtasks, dbtask)
			}

			return true
		})
		return err2
	})

	return dbtasks, err
}

///////////////////////////////////////////////////////////////////////////////
// Orchestrators

/*
SetOrchestrator adds a new orchestrator / updates value
*/
func (a Adapter) SetOrchestrator(id string, dbOrch structs.E2cOrchestrator) error {
	return setValue(id, dbOrch, dbE2cOrchestratorPREFIX)
}

/*
UpdateOrchestrator adds a new orchestrator / updates value
*/
func (a Adapter) UpdateOrchestrator(id string, dbOrch structs.E2cOrchestrator) error {
	return setValue(id, dbOrch, dbE2cOrchestratorPREFIX)
}

/*
ReadOrchestrator gets an orchestrator from database
*/
func (a Adapter) ReadOrchestrator(id string) (structs.E2cOrchestrator, error) {
	id = strings.Replace(id, dbE2cOrchestratorPREFIX, "", 1)

	dbOrch := &structs.E2cOrchestrator{}
	err := E2CODB.View(func(tx *buntdb.Tx) error {
		dborchStr, err := tx.Get(dbE2cOrchestratorPREFIX + id)
		if err != nil {
			log.Warn(pathLOG+"[ReadOrchestrator] Warning: Orchestrator not found in DB: ", err)
			return err
		}

		log.Trace(pathLOG + "[ReadOrchestrator] [E2cOrchestrator=" + dborchStr + "]")
		dbOrch, err = structs.StringToE2cOrchestrator(dborchStr)
		return err
	})

	return *dbOrch, err
}

/*
DeleteOrchestrator removes an orchestrator from database
*/
func (a Adapter) DeleteOrchestrator(id string) (string, error) {
	return deleteValue(id, dbE2cOrchestratorPREFIX)
}

/*
ReadAllOrchestrators Return all orchestrators from database
*/
func (a Adapter) ReadAllOrchestrators() ([]structs.E2cOrchestrator, error) {
	log.Trace(pathLOG + "[ReadAllOrchestrators] Getting All orchestrators ...")
	var dbOrchs []structs.E2cOrchestrator

	err := E2CODB.View(func(tx *buntdb.Tx) error {
		err2 := tx.Ascend("", func(key, value string) bool {
			log.Trace(pathLOG + ">>>> key: " + key + ", value: " + value)

			dborchStr, err := structs.StringToE2cOrchestrator(value)
			if err == nil && strings.HasPrefix(key, dbE2cOrchestratorPREFIX) {
				dbOrchs = append(dbOrchs, *dborchStr)
			}

			return true
		})
		return err2
	})

	return dbOrchs, err
}

/*
ReadAllOrchestratorsApps Return all orchestrators from database
*/
func (a Adapter) ReadAllOrchestratorsApps() ([]structs.E2cOrchestratorApps, error) {
	log.Trace(pathLOG + "[ReadAllOrchestratorsApps] Getting All orchestrators (apps version) ...")
	var dbOrchs []structs.E2cOrchestratorApps

	err := E2CODB.View(func(tx *buntdb.Tx) error {
		err2 := tx.Ascend("", func(key, value string) bool {
			log.Trace(pathLOG + ">>>> key: " + key + ", value: " + value)

			dborchStr, err := structs.StringToE2cOrchestratorApps(value)
			if err == nil && strings.HasPrefix(key, dbE2cOrchestratorPREFIX) {
				dbOrchs = append(dbOrchs, *dborchStr)
			}

			return true
		})
		return err2
	})

	return dbOrchs, err
}

///////////////////////////////////////////////////////////////////////////////
// Logs

/*
SetLog store log
*/
func (a Adapter) SetLog(s string) error {
	currentTime := time.Now()
	id := currentTime.Format("2006-01-02 15:04:05.000000000")

	return setValue(id, s, dbLogsPREFIX)
}

/*
ReadLogs Return last n logs
*/
func (a Adapter) ReadLogs(n int) ([]string, error) {
	log.Trace(pathLOG + "[ReadLogs] Getting logs ...")
	var dblogs []string

	err := E2CODB.View(func(tx *buntdb.Tx) error {
		err2 := tx.Ascend("", func(key, value string) bool {
			log.Trace(pathLOG + ">>>> key: " + key + ", value: " + value)

			if strings.HasPrefix(key, dbLogsPREFIX) {
				dblogs = append(dblogs, value)
			}

			return true
		})
		return err2
	})

	return dblogs, err
}

///////////////////////////////////////////////////////////////////////////////
// SOEObj

/*
SetSOEObj adds a new SoeObject / updates value
*/
func (a Adapter) SetSOEObj(id string, dbOrch structs.E2coSoeObject) error {
	return setValue(id, dbOrch, dbSoePREFIX)
}

/*
UpdateOrchestrator adds a new SoeObject / updates value
*/
func (a Adapter) UpdateSOEObj(id string, dbOrch structs.E2coSoeObject) error {
	return setValue(id, dbOrch, dbSoePREFIX)
}

/*
ReadSOEObj gets a SoeObject from database
*/
func (a Adapter) ReadSOEObj(id string) (structs.E2coSoeObject, error) {
	id = strings.Replace(id, dbSoePREFIX, "", 1)

	dbOrch := &structs.E2coSoeObject{}
	err := E2CODB.View(func(tx *buntdb.Tx) error {
		dborchStr, err := tx.Get(dbSoePREFIX + id)
		if err != nil {
			log.Warn(pathLOG+"[ReadSOEObj] Warning: SOEObj not found in DB: ", err)
			return err
		}

		log.Trace(pathLOG + "[ReadSOEObj] [E2cOrchestrator=" + dborchStr + "]")
		*dbOrch, err = structs.StringToE2coSoeObject(dborchStr)
		return err
	})

	return *dbOrch, err
}

/*
DeleteSOEObj removes a SoeObject from database
*/
func (a Adapter) DeleteSOEObj(id string) (string, error) {
	return deleteValue(id, dbSoePREFIX)
}

/*
ReadAllSOEObjs Return all SOEObjs from database
*/
func (a Adapter) ReadAllSOEObjs() ([]structs.E2coSoeObject, error) {
	log.Trace(pathLOG + "[ReadAllSOEObjs] Getting All SOEObjs ...")
	var dbOrchs []structs.E2coSoeObject

	err := E2CODB.View(func(tx *buntdb.Tx) error {
		err2 := tx.Ascend("", func(key, value string) bool {
			log.Trace(pathLOG + ">>>> key: " + key + ", value: " + value)

			dborchStr, err := structs.StringToE2coSoeObject(value)
			if err == nil && strings.HasPrefix(key, dbSoePREFIX) {
				dbOrchs = append(dbOrchs, dborchStr)
			}

			return true
		})
		return err2
	})

	return dbOrchs, err
}