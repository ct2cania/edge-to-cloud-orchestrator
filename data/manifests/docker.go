//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package manifests

// Docker Service
type DockerComposeService struct {
	Image    string   `yaml:"image"`
	Ports    []string `yaml:"ports"`
	Networks []string `yaml:"networks"`
	Deploy   struct {
		Replicas  int `yaml:"replicas"`
		Resources struct {
			Limits struct {
				Cpus   string `yaml:"cpus"`
				Memory string `yaml:"memory"`
			} `yaml:"limits"`
			Reservations struct {
				Cpus   string `yaml:"cpus"`
				Memory string `yaml:"memory"`
			} `yaml:"reservations"`
		} `yaml:"resources"`
	} `yaml:"deploy,omitempty"`
	Volumes     []string `yaml:"volumes"`
	Depends_on  []string `yaml:"depends_on"`
	Labels      []string `yaml:"labels"`
	Environment []string `yaml:"environment"`
}

// Docker Deployment
type DockerComposeDeployment struct {
	Version  string                          `yaml:"version"`
	Services map[string]DockerComposeService `yaml:"services"`
	Volumes  map[string]string               `yaml:"volumes"`
	Networks map[string]string               `yaml:"networks"`
}
