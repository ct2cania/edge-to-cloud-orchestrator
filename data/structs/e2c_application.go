//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 09 Apr 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package structs

/*

Applications definitions (JSON) examples:

------------------------------------
{
    "id": "",
    "created": "",
    "name": "",
    "description": "",
    "replicas": 1,
    "environment": [{
		"name": "",
		"value": ""
	}],
    "locations": [{
		"idE2cOrchestrator": "",
		"namespace": "",
		"description": ""
	}],
    "apps": [{ 
		"id": "",
        "name": "",
		"locations": [{
			"idE2cOrchestrator": "",
			"namespace": "",
			"description": ""
		}],
		"qos": [],
		"replicas": 1,
		"environment": [{
			"name": "",
			"value": ""
		}],
		"type": "", // stateless, function, default, job
		// estructura segun el tipo
		// containers: default
		"containers": [],
		// function: function
  		"functions": [],
        "deployment_info": {} // info de la aplicación ya desplegada
	 	}],
    "qos": [],
    "info": {
		"status": "",
   		"deployment_info": {} // puertos y demas historias 
	},
}
------------------------------------


Minimum application definition example:

1. 'docker' / 'k8s' application without QoS:

{
	"id": "asd9knu732sdcxkh99",
	"name": "nginx-app",
	"locations": [{"idE2cOrchestrator": "cluster2"}],
	"apps": [{
		"id": "fskhdsa8923k8sa",
		"name": "nginx",
		"containers": [{
			"image": "nginx",
			"ports": [{
				"port": 80,
				"protocol": "tcp"
				}],
			"service": {
				"expose": true
				}
			}]
		}]
}

2. 'docker' / 'k8s' application with QoS:

{
    "id": "asd9knu732sdcxkh99",
	"name": "nginx-app",
	"description": "nginx app with QoS definition deployed in multiple clusters"
    "locations": [],
    "apps": [{ 
		"id": "fskhdsa8923k8sa",
		"name": "nginx_1",
		"description": "nginx app deployed in Docker"
		"replicas": 1,
		"locations": [{"idE2cOrchestrator": "cluster3"}],
		"containers": [{
			"image": "nginx",
			"ports": [{
				"port": 80,
				"protocol": "tcp"
			}],
			"service": {
				"expose": true
			}
		}, 
		{ 
		"id": "jkljdsa98273123",
		"name": "nginx_2",
		"description": "nginx app deployed in Kubernetes"
		"replicas": 3,
		"locations": [{"idE2cOrchestrator": "cluster2"}],
		"containers": [{
			"image": "nginx",
			"ports": [{
				"port": 80,
				"protocol": "tcp"
			}],
			"service": {
				"expose": true
			}
		}],
		"qos": [{
			"name": "responseTime",
			"constraint": "responseTime < 1053769"
		}]
	}]
}

*/

/*
E2coApplication E2co Application
*/
type E2coApplication struct {
	ID         		string 					`json:"id"`
	Created         string    				`json:"created,omitempty"`
	Updated         string    				`json:"updated,omitempty"`
	Name            string    				`json:"name"`
	Description		string 					`json:"description,omitempty"`
	Status 			string 					`json:"status,omitempty"` 		// status
	DryRun       	bool                    `json:"dryrun,omitempty"`       // false or empty store and deploy. False store only
	// default vaules; used when no specific values are defined in applications
	Replicas        int       				`json:"replicas,omitempty"`
	Environment 	[]E2coEnvironmentVars	`json:"environment,omitempty"`
	Locations 		[]E2coLocation			`json:"locations,omitempty"`
	Qos             []SLADetailsGuarantee 	`json:"qos,omitempty"`
	MaxReplicas     int       				`json:"maxReplicas,omitempty"`	// optional
	// application definition
	Apps			[]E2coSubApp			`json:"apps,omitempty"`
	Info 			E2coAppInfo				`json:"info,omitempty"`
	// networks / soe
	//SoeObj 			E2coSoeObject			`json:"soeObj,omitempty"`
}

type E2coSubApp struct {
	ID         		string 						`json:"id"`
	ParentID        string 						`json:"parent"`
	Created         string    					`json:"created,omitempty"`
	Updated         string    					`json:"updated,omitempty"`
	Name            string    					`json:"name"`
	Description		string 						`json:"description,omitempty"`
	Type 			string 						`json:"type,omitempty"`
	// application specific values
	Replicas        int       					`json:"replicas,omitempty"`
	ReplicasType 	string 						`json:"replicasType"` // global, specific
	MaxReplicas     int       					`json:"maxReplicas,omitempty"`	// optional
	Environment 	[]E2coEnvironmentVars  		`json:"environment,omitempty"`
	Locations 		[]E2coLocation				`json:"locations,omitempty"`
	LocationsType 	string 						`json:"locationsType"` // global, specific
	Qos             []SLADetailsGuarantee 		`json:"qos,omitempty"`
	// App: containers, functions, singleapp, singlefunction
	Containers 		[]E2coAppContainer 			`json:"containers,omitempty"` 	// Containers field
	Functions		[]E2coAppFunction 			`json:"functions,omitempty"` 	// Functions field
	App 			E2coAppContainer			`json:"-"`
	Function		E2coAppFunction 			`json:"-"`
	// other common fields
	AssignedID 		string 						`json:"assignedID,omitempty"`	// used in docker deployments
	Status 			string 						`json:"status,omitempty"` 		// status
	YamlJSON		string 						`json:"yamlJSON,omitempty"`		// optional
	Info 			E2coTaskStatusInfo   		`json:"info,omitempty"`
	// expose?
	Service			E2coAppService				`json:"service,omitempty"`
	// labels
	Labels			[]E2coAppLabel				`json:"labels,omitempty"`
	// Volumes
	Volumes     	[]E2coAppContainerVolumes	`json:"volumes,omitempty"`
	// NodeSelector
	NodeSelector    map[string]string 			`json:"nodeSelector,omitempty"`
	NodeName    	string 						`json:"nodeName,omitempty"`
}

type E2coAppContainer struct {
	Name        		string                      `json:"name,omitempty"`
	Image       		string                      `json:"image,omitempty"`
	ImageRepoName       string                      `json:"repo_user,omitempty"`
	ImageRepoPassword	string                      `json:"repo_password,omitempty"`
	NetworkName			string              		`json:"network_name,omitempty"`
	Ports       		[]E2coAppContainerPorts		`json:"ports,omitempty"`
	Volumes     		[]E2coAppContainerVolumes	`json:"volumes,omitempty"`
	Environment 		[]E2coEnvironmentVars	  	`json:"environment,omitempty"`
	EnvDocker       	[]E2coEnvironmentVars       `json:"env_docker,omitempty"`
	Command     		[]string                    `json:"command,omitempty"`
	Args        		[]string                    `json:"args,omitempty"`
	Hw 					E2coAppkHwProps 			`json:"hw,omitempty"`
	Labels				[]E2coAppLabel				`json:"labels,omitempty"`
	ImagePullPolicy		string    					`json:"imagePullPolicy,omitempty"`
	ImagePullSecret 	string    					`json:"imagePullSecret,omitempty"`
	ImagePullSecrets 	[]map[string]string 		`json:"imagePullSecrets,omitempty"`
	RunAsPrivileged 	bool						`json:"runAsPrivileged,omitempty"`
}

type E2coAppFunction struct {
	Runtime      string `json:"runtime,omitempty"`
	Timeout      string `json:"timeout,omitempty"`
	Handler      string `json:"handler,omitempty"`
	FunctionType string `json:"functionType,omitempty"`
	Function     string `json:"function,omitempty"`
}

type E2coAppService struct {
	Port 		int    		`json:"port,omitempty"`
	URL 		string    	`json:"url,omitempty"`
	Expose		bool   		`json:"expose,omitempty"`
}

type E2coEnvironmentVars struct {
	Name  string `json:"name,omitempty"`
	Value string `json:"value,omitempty"`
}

type E2coLocation struct {
	Description 		string 		`json:"description"`
	NameSpace         	string  	`json:"namespace"`
	IDE2cOrchestrator 	string  	`json:"idE2cOrchestrator,omitempty"`
	Nodes				[]string 	`json:"nodes,omitempty"`
	CurrentNameSpace   	string  	`json:"currentNamespace"`
}

type E2coAppInfo struct {	// Information about the application / service
	Status 	string 		`json:"status,omitempty"`
	Message string  	`json:"message,omitempty"`
	Type 	string 		`json:"type,omitempty"`
	IDObj	string 		`json:"idObj,omitempty"`
	Obj		interface{} `json:"obj,omitempty"`
}

type E2coAppLabel struct {
	Key 	string 	`json:"key,omitempty"`
	Value 	string	`json:"value,omitempty"`
}

type E2coAppkHwProps struct {
	Memory 	string    `json:"memory,omitempty"`
	Cpu     string    `json:"cpu,omitempty"`
}

type E2coAppContainerPorts struct {
	Port 			int    		`json:"port,omitempty"`
	HostPort		int    		`json:"hostPort,omitempty"`
	ContainerPort 	int    		`json:"containerPort,omitempty"`
	Protocol    	string 		`json:"protocol,omitempty"`
}

type E2coAppContainerVolumes struct {
	Name      	string 		`json:"name,omitempty"`
	MountPath 	string 		`json:"mounthPath,omitempty"`
	ReadOnly  	bool   		`json:"readOnly,omitempty"`
}

///////////////////////////////////////////////////////////////////////////////
// DEFAULT VALUES

const DEFAULT_APP_PORT_PROTOCOL string = "tcp"
const DEFAULT_APP_LOCATION_NAMESPACE string = "default"
const DEFAULT_APP_REPLICAS int = 1
const DEFAULT_APP_MAX_REPLICAS int = 4
const DEFAULT_APP_TYPE string = "default"

///////////////////////////////////////////////////////////////////////////////

/*
E2coTaskStatusInfo status of the application
*/
type E2coTaskStatusInfo struct {
	Status 	string 	`json:"status,omitempty"`
	Message string  	`json:"message,omitempty"`
	Type 	string 		`json:"type,omitempty"`
	IDObj	string 		`json:"idObj,omitempty"`
	Obj		interface{} `json:"obj,omitempty"`
}
