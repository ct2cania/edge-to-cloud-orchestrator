//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package structs

///////////////////////////////////////////////////////////////////////////////
// K8S / MICROK8s / K3s / OPENSHIFT structs definitions

// K8S DEPLOYMENT

/*
K8sDeploymentContainerPorts ...
*/
type K8sDeploymentContainerPorts struct {
	ContainerPort int    `json:"containerPort,omitempty"`
	HostPort      int    `json:"hostPort,omitempty"`
	Protocol      string `json:"protocol,omitempty"`
}

/*
K8sDeploymentContainerEnv ...
*/
type K8sDeploymentContainerEnv struct {
	Name  string `json:"name,omitempty"`
	Value string `json:"value,omitempty"`
}

/*
K8sDeploymentContainerResources ...
*/
type K8sDeploymentContainerResources struct {
	Requests K8sDeploymentContainerHWResources `json:"requests,omitempty"`
	Limits   K8sDeploymentContainerHWResources `json:"limits,omitempty"`
}

/*
K8sDeploymentContainerHWResources ...
*/
type K8sDeploymentContainerHWResources struct {
	Memory string `json:"memory,omitempty"`
	Cpu    string `json:"cpu,omitempty"`
}

/*
K8sDeploymentContainerVolumeMounts ...
*/
type K8sDeploymentContainerVolumeMounts struct {
	Name      string `json:"name,omitempty"`
	MountPath string `json:"mountPath,omitempty"`
	ReadOnly  bool   `json:"readOnly,omitempty"`
}

/*
K8sDeploymentContainer ...
*/
type K8sDeploymentContainer struct {
	Image           	string                               `json:"image,omitempty"`
	Name            	string                               `json:"name,omitempty"`
	//ImageRepoName       string         						 `json:"imageRepoName,omitempty"`
	//ImageRepoPassword	string              				 `json:"imageRepoPassword,omitempty"`
	ImagePullPolicy 	string                               `json:"imagePullPolicy,omitempty"`
	ImagePullSecrets 	[]map[string]string 		  		 `json:"imagePullSecrets,omitempty"`
	ImagePullSecret  	[]string 		   		  			 `json:"imagePullSecret,omitempty"`
	Ports           	[]K8sDeploymentContainerPorts        `json:"ports,omitempty"`
	Env             	[]K8sDeploymentContainerEnv          `json:"env,omitempty"`
	Command         	[]string                             `json:"command,omitempty"`
	Args            	[]string                             `json:"args,omitempty"`
	Resources       	K8sDeploymentContainerResources      `json:"resources,omitempty"`
	VolumeMounts    	[]K8sDeploymentContainerVolumeMounts `json:"volumeMounts,omitempty"`
}

/*
K8sDeploymentVolume ...
*/
type K8sDeploymentVolume struct {
	Name                  string `json:"name,omitempty"`
	PersistentVolumeClaim struct {
		ClaimName string `json:"claimName,omitempty"`
	} `json:"persistentVolumeClaim,omitempty"`
	Secret struct {
		SecretName string `json:"secretName,omitempty"`
	} `json:"secret,omitempty"`
	ConfigMap struct {
		Name string `json:"name,omitempty"`
	} `json:"configMap,omitempty"`
}

/*
K8sDeployment ...
*/
type K8sDeployment struct {
	APIVersion string `json:"apiVersion,omitempty"`
	Kind       string `json:"kind,omitempty"`
	Metadata   struct {
		Name      string                 `json:"name,omitempty"`
		Labels    map[string]interface{} `json:"labels,omitempty"`
		Namespace string                 `json:"namespace,omitempty"`
	} `json:"metadata,omitempty"`
	Spec struct {
		Strategy             interface{} `json:"strategy,omitempty"`
		Replicas             int         `json:"replicas,omitempty"`
		RevisionHistoryLimit int         `json:"revisionHistoryLimit,omitempty"`
		Selector             struct {
			//MatchLabels struct {
			//	App 			string 						`json:"app,omitempty"`
			//} `json:"matchLabels,omitempty"`
			MatchLabels map[string]interface{} `json:"matchLabels,omitempty"`
		} `json:"selector,omitempty"`
		Template struct {
			Metadata struct {
				/*Labels struct {
					App 		string 						`json:"app,omitempty"`
				} `json:"labels,omitempty"`*/
				Labels map[string]interface{} `json:"labels,omitempty"`
			} `json:"metadata,omitempty"`
			Spec struct {
				ImagePullSecrets []map[string]string 	  `json:"imagePullSecrets,omitempty"`
				Containers       []K8sDeploymentContainer `json:"containers,omitempty"`
				//ImagePullSecrets []string                 `json:"imagePullSecrets,omitempty"` 
				//ImagePullSecrets map[string]string 		  `json:"imagePullSecrets,omitempty"`
				//ImagePullSecret  []string 		   		  `json:"imagePullSecret,omitempty"`
				Volumes          []K8sDeploymentVolume    `json:"volumes,omitempty"`
				NodeSelector     map[string]interface{}   `json:"nodeSelector,omitempty"`
			} `json:"spec,omitempty"`
		} `json:"template,omitempty"`
	} `json:"spec,omitempty"`
}

// K8S JOB

/*
K8sJob ...
*/
type K8sJob struct {
	APIVersion string `json:"apiVersion,omitempty"`
	Kind       string `json:"kind,omitempty"`
	Metadata   struct {
		Name string `json:"name,omitempty"`
	} `json:"metadata,omitempty"`
	Spec struct {
		Replicas             int `json:"replicas,omitempty"`
		RevisionHistoryLimit int `json:"revisionHistoryLimit,omitempty"`
		Selector             struct {
			MatchLabels struct {
				App string `json:"app,omitempty"`
			} `json:"matchLabels,omitempty"`
		} `json:"selector,omitempty"`
		Template struct {
			Metadata struct {
				Labels struct {
					App string `json:"app,omitempty"`
				} `json:"labels,omitempty"`
			} `json:"metadata,omitempty"`
			Spec struct {
				Containers []K8sDeploymentContainer `json:"containers,omitempty"`
			} `json:"spec,omitempty"`
		} `json:"template,omitempty"`
	} `json:"spec,omitempty"`
}

// K8S SERVICE

/*
K8sServicePort ...
*/
type K8sServicePort struct {
	Name       string `json:"name,omitempty"`
	Port       int    `json:"port,omitempty"`
	Protocol   string `json:"protocol,omitempty"`
	TargetPort int    `json:"targetPort,omitempty"`
	NodePort   int    `json:"nodePort,omitempty"`
}

/*
K8sService ...
*/
type K8sService struct {
	APIVersion string `json:"apiVersion,omitempty"`
	Kind       string `json:"kind,omitempty"`
	Metadata   struct {
		Name   string                 `json:"name,omitempty"`
		Labels map[string]interface{} `json:"labels,omitempty"`
		//Labels struct {
		//	App string `json:"app,omitempty"`
		//} `json:"labels,omitempty"`
		Namespace string `json:"namespace,omitempty"`
	} `json:"metadata,omitempty"`
	Spec struct {
		//Selector struct {
		//	App     string `json:"app,omitempty"`
		//	PodName string `json:"pod-name,omitempty"`
		//} `json:"selector,omitempty"`
		Selector        map[string]interface{} `json:"selector,omitempty"`
		ExternalIPs     []string               `json:"externalIPs,omitempty"`
		Ports           []K8sServicePort       `json:"ports,omitempty"`
		Type            string                 `json:"type,omitempty"`
		SessionAffinity string                 `json:"sessionAffinity,omitempty"`
		ClusterIP       string                 `json:"clusterIP,omitempty"`
	} `json:"spec,omitempty"`
	Status struct {
		LoadBalancer struct {
			Ingress []string `json:"ingress,omitempty"`
		} `json:"loadBalancer,omitempty"`
	} `json:"status,omitempty"`
}

// K8S ROUTE

/*
K8sRoute ...
*/
type K8sRoute struct {
	APIVersion string `json:"apiVersion,omitempty"`
	Kind       string `json:"kind,omitempty"`
	Metadata   struct {
		Name      string `json:"name,omitempty"`
		Namespace string `json:"namespace,omitempty"`
	} `json:"metadata,omitempty"`
	Spec struct {
		Host string `json:"host,omitempty"`
		Port struct {
			TargetPort string `json:"targetPort,omitempty"`
		} `json:"port,omitempty"`
		To struct {
			Kind string `json:"kind,omitempty"`
			Name string `json:"name,omitempty"`
		} `json:"to,omitempty"`
	} `json:"spec,omitempty"`
}

// K8S SCALE

/*
K8sScale ...
*/
type K8sScale struct {
	APIVersion string `json:"apiVersion,omitempty"`
	Kind       string `json:"kind,omitempty"`
	Metadata   struct {
		Name              string `json:"name,omitempty"`
		Namespace         string `json:"namespace,omitempty"`
		SelfLink          string `json:"selfLink,omitempty"`
		UID               string `json:"uid,omitempty"`
		ResourceVersion   string `json:"resourceVersion,omitempty"`
		CreationTimestamp string `json:"creationTimestamp,omitempty"`
	} `json:"metadata,omitempty"`
	Spec struct {
		Replicas int `json:"replicas,omitempty"`
	} `json:"spec,omitempty"`
	Status struct {
		Replicas int    `json:"replicas,omitempty"`
		Selector string `json:"selector,omitempty"`
	} `json:"status,omitempty"`
}

// K8S POD

/*
K8sPod ...
*/
type K8sPod struct {
	Name       string `json:"name,omitempty"`
	IP         string `json:"ip,omitempty"`         // IP accessed by external apps
	HostIP     string `json:"hostIp,omitempty"`     // node IP
	PodIP      string `json:"podIp,omitempty"`      // internal IP created by Kubernetes / Openshift
	Status     string `json:"status,omitempty"`     // running, unknown
	Protocol   string `json:"protocol,omitempty"`   // TCP
	Port       int    `json:"port,omitempty"`       // port exposed in Kubernetes / Openshift
	TargetPort int    `json:"targetPort,omitempty"` // application port
}

// K8S POD PATCH

/*
K8sPodPatchLine ...
*/
type K8sPodPatchLine struct {
	Op    string `json:"op,omitempty"`
	Path  string `json:"path,omitempty"`
	Value string `json:"value,omitempty"`
}

/*
K8sDeploymentResourcesPatch ...
*/
type K8sDeploymentResourcesPatch struct {
	Spec   struct {
		Template struct {
			Spec struct {
				Containers []K8sDeploymentContainerResourcesPatch  `json:"containers,omitempty"`
				/*struct {
					Name string `json:"name,omitempty"`
					Resources struct {
						Limits struct {
							Cpu string `json:"cpu,omitempty"`
							Memory string `json:"memory,omitempty"`
						} `json:"limits,omitempty"`
						Requests struct {
							Cpu string `json:"cpu,omitempty"`
							Memory string `json:"memory,omitempty"`
						} `json:"requests,omitempty"`
					} `json:"resources,omitempty"`
				} `json:"containers,omitempty"`*/
			} `json:"spec,omitempty"`
		} `json:"template,omitempty"`
	} `json:"spec,omitempty"`
}

/*
K8sDeploymentContainerResourcesPatch ...
*/
type K8sDeploymentContainerResourcesPatch struct {
	Name string `json:"name,omitempty"`
	Resources struct {
		Limits struct {
			Cpu string `json:"cpu,omitempty"`
			Memory string `json:"memory,omitempty"`
		} `json:"limits,omitempty"`
		Requests struct {
			Cpu string `json:"cpu,omitempty"`
			Memory string `json:"memory,omitempty"`
		} `json:"requests,omitempty"`
	} `json:"resources,omitempty"`
}


/*
PatchResourcesObj ...
*/
type PatchResourcesObj struct {
	Spec   PatchResourcesTemplateObj `json:"spec,omitempty"`
}

/*
PatchResourcesTemplateObj ...
*/
type PatchResourcesTemplateObj struct {
	Template   PatchResourcesSpecObj `json:"template,omitempty"`
}

/*
PatchResourcesSpecObj ...
*/
type PatchResourcesSpecObj struct {
	Spec   PatchResourcesContainersObj `json:"spec,omitempty"`
}

/*
PatchResourcesContainersObj ...
*/
type PatchResourcesContainersObj struct {
	Containers   []PatchResourcesContainerObj `json:"containers,omitempty"`
}

/*
PatchResourcesContainerObj ...
*/
type PatchResourcesContainerObj struct {
	Name string `json:"name,omitempty"`
	Image string `json:"image,omitempty"`
	Resources       K8sDeploymentContainerResources      `json:"resources,omitempty"`
}
