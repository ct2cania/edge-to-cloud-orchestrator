//
// Copyright 2020 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 12 Jan 2021
// Updated on 04 Aug 2021
//
// @author: ATOS
//
package structs

///////////////////////////////////////////////////////////////////////////////
// SERVERLESS functions structs definitions

// KUBELESS

/*
KubelessFunctionStruct ...
	JSON EXAMPLE:
		{
			"apiVersion": "kubeless.io/v1beta1",
			"kind": "Function",
			"metadata": {
				"name": "get-python",
				"namespace": "default",
				"label": {
					"created-by": "kubeless",
					"function": "get-python"
				}
			},
			"spec": {
				"runtime": "python2.7",
				"timeout": "180",
				"handler": "helloget.foo",
				"deps": "",
				"checksum": "sha256:d251999dcbfdeccec385606fd0aec385b214cfc74ede8b6c9e47af71728f6e9a",
				"function-content-type": "text",
				"function": "def foo(event, context):\n    return \"hello world\"\n"
			}
		}
*/
type KubelessFunctionStruct struct {
	APIVersion string `json:"apiVersion,omitempty"`
	Kind       string `json:"kind,omitempty"`
	Metadata   struct {
		Name      string `json:"name,omitempty"`
		NameSpace string `json:"namespace,omitempty"`
		Label     struct {
			CreatedBy string `json:"created-by,omitempty"`
			Function  string `json:"function,omitempty"`
		} `json:"label,omitempty"`
	} `json:"metadata,omitempty"`
	Spec struct {
		Runtime             string `json:"runtime,omitempty"`
		Timeout             string `json:"timeout,omitempty"`
		Handler             string `json:"handler,omitempty"`
		Deps                string `json:"deps,omitempty"`
		Checksum            string `json:"checksum,omitempty"`
		FunctionContentType string `json:"function-content-type,omitempty"`
		Function            string `json:"function,omitempty"`
	} `json:"spec,omitempty"`
}
